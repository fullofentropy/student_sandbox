from animal import Mammal

class People(Mammal):
    def __init__(self):
        print("I am human with only 2 legs")
        super().__init__('Human')
        print("Test inside People")

    def same_name(self):
        print("I am a human")
        super().same_name()


# adam = People()

# adam.same_name()
