# this program demonstrates object unpickling
import pickle

# main function
def main():
    # Indicate the the end of the file
    end_of_file = False

    # Open a file for binary reading
    input_file = open('information.dat', 'rb')

    # Read to the end of the file
    while not end_of_file:
        try:
            # Unpickle/Deserialize the next object
            person = pickle.load(input_file)

            # Display the object
            display_data(person)

        except EOFError:
            # Set the flag to indicate the end
            # of the file has been reached
            end_of_file = True
    
    # Close the file
    input_file.close()

def display_data(person):
    """
    The display_data function displays the person data
    in the dictionary that was passed as an argument
    """
    print('Name:', person['name'])
    print('Age:', person['age'])
    print('Weight:', person['weight'])
    print()

    # print(f"Name: {person['name']} \nAge: {person['age']} \nWeight: {person['weight']}")

# Call the main function
main()    








