# Serializing Objects
# Serializing an object is the process of converting the object to a stream of 
# bytes that can be saved to a file for later retrieval.  In Python, a type of object
# serialization is called pickling

#This program demonstrates object pickling
import pickle


# main function
def main():
    # controls loop repetition
    again = 'y'

    # open a file for binary writing
    output_file = open('information.dat', 'wb')

    # Get data until the user wants to stop
    while again.lower() == 'y':
        # Get data about a person and save it.
        save_data(output_file)

        # Does the user want to enter more data?
        again = input('Enter more data? (y/n): ') or 'y'

    #close the file
    output_file.close()



def save_data(file):
    """
    The save_data function gets data about a person,
    stores it in a dictionary, and then pickles the 
    dictionary to the specified file
    """

    # Create an empty dictionary
    person = {}

    # Get the data for a person and store it in a dictionary
    person['name'] = input('Name: ')
    person['age'] = input('age: ')
    person['weight'] = float(input('Weight: '))

    # person.update({'name': input('Name: '), 'age': input('Age: '), 'weight': float(input('Weight: '))})

    # Pickle the dictionary
    pickle.dump(person, file)

# Call the main funciton
main()






