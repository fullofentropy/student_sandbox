
import hashlib

password = b'mypassword'

# loading an object hold a hash
m = hashlib.md5()

#--- hasing a byte string
m.update(password)

#-- printing the hash
print(m.digest())
#printing the hex of the hash
print(m.hexdigest())

#-- this could be data received from the user

x = hashlib.md5()
x.update(bytes(input("Enter the Password: "),'utf-8'))
print(x.digest())

if m.digest() == x.digest():
    print('access granted')
else:
    print("access denied")