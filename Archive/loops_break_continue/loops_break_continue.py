"""
break - breaks out of the innermost enclosing loop

continue - with the next iteration of a loop

"""
import codecs
import random

def example1():
    
    stolen_passwords = ['qvqgur', 'uhagre2', 'cnffjbeq', 'e@aq0z']
    common_passwords = ['password1', 'hunter2', '12345678']

    for password in stolen_passwords:
        decrypted_password = codecs.encode(password, 'rot_13')
        print(f"checking if {password} is in the common password list")
        if decrypted_password in common_passwords:
            print(f'found a password: {decrypted_password}.')
            break



def example2():
    words = ['alpha', 'beta', 'charlie', 'delta', 'echo', 'foxtrot', 'golf']
    numbers = [1, 2, 3, 4]

    for word in words:
        print(f"current word is {word}")
        for number in numbers:
            print(f'current number is {number} for word {word}')
            if number == random.randint(1, 3):
                print(f'number collision, breaking loop')
                
            
def example3():
    for letter in 'Frog':
        if letter == "g":
            continue

        print(letter, end='')


def fizzbuzz():
    pass




