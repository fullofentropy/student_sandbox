import re

# compile our re
the_string = "Can't stop, won't stop"
the_string2 = "stop the music"
the_string3 = "Never ending story"

# re.search searches the entire string
# re.match searches at the start of the string and stops if no match is immediately found
print('-----')
pattern = re.compile(r'stop')
results = pattern.search(the_string)
print(results)
print('-----')

# search for something in a loop.
print('-----FOR LOOP USING "None" RETURN VALUE TO DETERMIN NO MATCH EXISTS.')
for _ in [the_string, the_string2, the_string3]:
    temp_result = pattern.search(_)
    if temp_result:
        print(f"""We have a match using:
        Token:{pattern}
        Value:{temp_result.group()}
        Spanning Location:{temp_result.span()}
        {temp_result.group()}""")
    else:
        print(f"T.T NO MATCH in string -->{_}<--")

 # Note: (?!-) is a negative look ahead,  (?<!-) is a negative look behind 
    # Lookaround    Name    What it Does
    # (?=foo)    Lookahead    Asserts that what immediately follows the current position in the string is foo
    # (?<=foo)    Lookbehind    Asserts that what immediately precedes the current position in the string is foo
    # (?!foo)    Negative Lookahead    Asserts that what immediately follows the current position in the string is not foo
    # (?<!foo)    Negative Lookbehind    Asserts that what immediately precedes the current position in the string is not foo

print('-----LOOK BEHIND EXAMPLE-------')
results2 = re.search(r'\w+(?<=Can\'t stop)',the_string)
print(results2)

print('-----LOOK LOOK AHEAD EXAMPLE WITH GROUPING-------')
results3 = re.search(r'(st)o(p)(?= the)',the_string2)
print('====re.search====')
print(results3)
print('====start/end====')
print(results3.start())
print(results3.end())
print('====span====')
print(results3.span())
print(results3.span()[0])
print(results3.span()[1])
print('====group====')
print(results3.group())
print(results3.group(1))
print(results3.group(2))

