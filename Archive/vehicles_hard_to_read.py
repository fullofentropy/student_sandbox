# from vehicles_package import Car, SUV, Truck, Engine
from vehicles_package import *

# https://docs.python.org/3/library/csv.html
import csv, random, re, sys, os

from vehicles_package.automobile import Automobile

def main():
    data_file = 'Book1.csv'
    myList = []
    myCarDictData = {}

    searchKey, vehicleClassIndex, vehicleInstanceIndex, vehicleClassInstanceDict = 'Type'.lower(), 0, 1,  {'Car':   [Car, []],'SUV':   [SUV, []], 'Truck': [Truck, []]  }

    # Open the file for csv processing
    with open('Book1.csv', mode='r') as csv_file:
        readinDict = csv.DictReader(csv_file)
        for dictRow in readinDict:
            dictRow = { key.lower(): value for key, value in dictRow.items() }  
            if dictRow[searchKey] in vehicleClassInstanceDict.keys():
                vehicleClassInstanceDict[dictRow[searchKey]][vehicleInstanceIndex].append(vehicleClassInstanceDict[dictRow[searchKey]][vehicleClassIndex](**dictRow))                
                print(vehicleClassInstanceDict[dictRow[searchKey]][vehicleInstanceIndex][-1])

    for key1, valueList in vehicleClassInstanceDict.items():
        for instance in valueList[1]:
            print(f'''This is an instance of 
            type: {instance.__class__.__name__} 
            with make: {instance.get_make()}
            model: {instance.get_model()}
            engine: {instance.get_engine()}''')


# Call the main
main()            


