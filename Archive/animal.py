class Mammal():
    __x1 = 4
    _x2 = __x1 + 4 * 5

    def __init__(self, mammalName):
        print(mammalName, 'is a warm-blooded animal.')
        print("Test inside Mammal")

    def printMe2(self):
        print('me2 me2 me2')
    
    def getPrivateVar(self):
        return self.__x1

    def setPrivateVar(self, myVal):
        self.__x1 = myVal
    
    def same_name(self):
        print("I am a Mammal")