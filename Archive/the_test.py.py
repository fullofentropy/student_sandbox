class Teacher(object):
    def __init__(self, name, college):
        self.name = name
        self.college = college

    def __str__(self):
        return f"{self.name} teaches at {self.college}"

    def salary(self):
        print(f"{self.name} is paid $63,645.")
    
    def college_info(self):
        print((f"{self.name} works at {self.college}"))

class PhysicsTeacher(Teacher):
    def __init__(self, name3, college3, speciality3):
        Teacher.__init__(self, name=name3, college=college3)
        self.subject = "Physics"
        self.speciality = speciality3

    def __str__(self):
        return super().__str__() + f", teaching the subject {self.subject} with a focus on {self.speciality}"

    def salary(self):
        salary_value = ""
        while not salary_value:
            salary_value = input(f"What is the salary for {self.name}?")
            try:
                self.salary_value = int(salary_value)
            except ValueError:
                print("That is not a valid value for the salary.  Please try again.")
                salary_value = ""     

jerry = PhysicsTeacher("Jerry", "State", "Asteroids")
print(jerry)         # Example 1
jerry.salary()       # Example 2
jerry.college_info() # Example 3   