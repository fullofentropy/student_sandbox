class Engine:
    """
    I reperesent a generic engine
    """
    def __init__(self, fuel='gas', cylinders=4) -> None:
        self.set_fuel_type(fuel)
        self.set_cylinders(cylinders)

    def __str__(self):
        return f'My engine has {self.cylinders} cylinders and I run on {self.fuel} fuel.'

    def set_cylinders(self, cylinders):
        try:
            self.cylinders = int(cylinders)
        except:
            cylinders = input("Please provide an integer value for number of cylindars. -->")
            self.set_cylinders(cylinders)

    def set_fuel_type(self, fuel):
        if type(fuel) is not str:
            self.fuel = input("What type of fuel does this vehicle use? -->")
        else:
            self.fuel = fuel
