from .engine import Engine
from random import randint, choice

myFuel = ['airman',
          'sailor',
          'soldier',
          'gas',
          'diesel']

class Automobile:
    # The __init__ method accepts arguments for the 
    # make, model, mileage, and price. It initializes
    # the data attributes with these values
    def __init__(self, make, model, mileage, price, engine=None, **kwargs):
        self.__make = make
        self.__model = model
        self.__mileage = mileage
        self.__price = price
        # This is expected to assign a class Engine to variable engine
        if engine == None:
            self.__engine = Engine(choice(myFuel),randint(1,10))
        
    def __str__(self):
        return f"I am a {self.__make}, {self.__model}"
    # The following methods are mutators for the 
    # class's data attributes
    def set_make(self, make):
        self.__make = make
    
    def set_model(self, model):
        self.__model = model
    
    def set_mileage(self, mileage):
        self.__mileage = mileage
    
    def set_price(self, price):
        self.__price = price

    def set_engine(self, fuel, cyl):
        self.__engine = Engine(fuel, cyl)

    # The following methods are the accessors
    # for the class's data attributes
    def get_make(self):
        return self.__make
    
    def get_model(self):
        return self.__model
    
    def get_mileage(self):
        return self.__mileage

    def get_price(self):
        return self.__price

    def get_engine(self):
        return self.__engine