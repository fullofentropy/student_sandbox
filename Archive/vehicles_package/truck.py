from .automobile import Automobile


class Truck(Automobile):
    # The __init__ method accepts arguments for the
    # Truck's make, model, mileage, price, and drive type
    def __init__(self, make, model, mileage, price, drive_type, engine=None, **kwargs):
        # Call the superclass's __init__ method and pass
        # the required arguments. 
        super().__init__(make, model, mileage, price, engine)

        # Initialize the __drive_type attribute
        self.__drive_type = drive_type

    # The set_drive_type method is the mutator for the
    # __drive_type attribute
    def set_drive_type(self, drive_type):
        self.__drive_type = drive_type
    
    # The get_drive_type method is the accessor for the
    # __drive_type attribute
    def get_drive_type(self):
        return self.__drive_type