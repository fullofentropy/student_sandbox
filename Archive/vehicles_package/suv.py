from .automobile import Automobile

class SUV(Automobile):
    # The __init__ method accepts arguments for the
    # SUV's make, model, mileage, price, and passenger capacity
    def __init__(self, make, model, mileage, price, pass_cap, engine=None, **kwargs):
        # Call the superclass's __init__ method and pass
        # the required arguments
        super().__init__(make, model, mileage, price, engine)

        # Initialize the __pass_cap attribute
        self.__pass_cap = pass_cap

    # The set_pass_cap method is the mutator for the 
    # __pass_cap attribute
    def set_pass_cap(self, pass_cap):
        self.__pass_cap = pass_cap

    # The get_pass_cap method is the accessor for the
    # __pass_cap attribute
    def get_pass_cap(self):
        return self.__pass_cap