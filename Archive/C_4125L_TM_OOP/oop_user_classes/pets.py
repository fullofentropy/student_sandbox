from animal import Mammal

class Cat(Mammal):
    def __init__(self):
        print("Cat has four legs.")
        super().__init__('Cat')
        print("Test inside Cat")

class Dog(Mammal):
    def __init__(self):
        print("Dog has four legs.")
        super().__init__('Dog')
        print("Test inside Dog")
        print("Test inside Dog")

class GoldenRetriever(Dog):
    def __init__(self):
        print("I am a Golden Retriever.")
        super().__init__()
        print("I am inside the GR")

    def printMe(self):
        print('me me me')

class Persians(Cat):
    def __init__(self):
        print("I am a Persian.")
        super().__init__()
        print("I am alive!")
