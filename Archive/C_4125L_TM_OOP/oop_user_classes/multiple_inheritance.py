class Mammal():
    x1 = 4

    def __init__(self, mammalName):
        print(mammalName, 'is a warm-blooded animal.')
        print("Test inside Mammal")

    def printMe2(self):
        print('me2 me2 me2')
        # super().__init__(mammalName)

class People(Mammal):
    def __init__(self):
        print("I am human with only 2 legs")
        super().__init__('Human')


class Cat(Mammal):
    def __init__(self):
        print("Cat has four legs.")
        super().__init__('Cat')
        print("Test inside Cat")

class Dog(Mammal):
    def __init__(self):
        print("Dog has four legs.")
        super().__init__('Dog')
        print("Test inside Dog")
        print("Test inside Dog")

class GoldenRetriever(Dog):
    def __init__(self):
        print("I am a Golden Retriever.")
        super().__init__()
        print("I am inside the GR")

    def printMe(self):
        print('me me me')
    

# p1 = People()
# print(p1.x1)
fred = GoldenRetriever()

print()
print(fred.x1)
fred.printMe()
fred.printMe2()



# d1 = Dog()
# print()
# c1 = Cat()

