class Example:
    """
    This is an example class
    """
    GENERAL_ERROR_FLOAT = 'Invalid number entered, please try again.'
    def __init__(self, number = None, name = 'bill'):
        """
        This is the initialization method that is run
        when creating an object from this class.

        attributes:
        name: this is an example name
        _number: this is a protected attribute number

        methods:
        setNumber:

        _displayName:
        """
        self.testnumber = 1
        self.name = name
        self.setNumber(number)
        self._displayName()

    def divisible_by_number(self, num, testnumber):
        try:
            testnumber / num
        
        except ZeroDivisionError:
            print('Can not divide by zero, try entering a number other than zero')
            self.setNumber(input('new number: '))

            
        

    def setNumber(self, num):
        """
        This sets the number as a float and only a float.
        """
        valueAcceptable = False
        while valueAcceptable == False:
            try:  
                self._number = float(num)
                self.divisible_by_number(self._number, self.testnumber)
                valueAcceptable = True

            except ValueError:
                # TODO:  add specific exceptions   
                print(self.GENERAL_ERROR_FLOAT)
                num = input("Please Enter a number: ")

            except TypeError:
                if num == None:
                    self._number = None
                    valueAcceptable = True
                else:    
                    print(self.GENERAL_ERROR_FLOAT)
                    num = input("Please Enter a number: ")


    def _displayName(self):
        print(f"Hello, {self.name}")
    
    def getNumber(self):
        return self._number


# print(a.getNumber())
# a.setNumber(5)
# print(a.getNumber())

# args = (0, 'Ava')


# a = Example(name = 'Ava', number=33)

# mykeywords = {'number' : "none", 'name' : "Bill"}
# a = Example(**mykeywords)

# print(a.getNumber())

# instanceExample = Example(45, "fred")
# print(instanceExample.getNumber())

# for _ in range(4):
#     print(_)

print()
x = iter([1,'moew',5,44,556,'meow333'])
try:
    while True:
        print(next(x))

except StopIteration:
    print('all done')

print(len('hello'))


