class Person:

    def __init__(self, firstname, lastname, birthday, age, email):
        self.firstName = firstname
        self.lastName = lastname
        self.birthday = birthday
        self.age = age
        self.email = email

    def printDetails(self):
        print(f"First Name: {self.firstName}")
        print(f"Last Name: {self.lastName}")
        print(f"birthday: {self.birthday}")
        print(f"email: {self.email}")

x = Person("Richard", "Smith", '04JUL2000', 33, 'hello@me.com')
x.printDetails()