
class Person():
    """ A sample of what a class looks like."""
    def __init__(self, fname='dummyf',lname='dummyl'):
        self._first_name = fname
        self._last_name = lname
        self.print_name()

    def print_name(self):
        print(f"My name is {self._last_name}, {self._first_name} {self._last_name}")

    def set_fname(self, new_name):
        self._first_name = new_name

    def set_lname(self, new_name):
        self._last_name = new_name

    def get_fname(self):
        return self._first_name

    def get_lname(self):
        return self._last_name


test = Person()
john_smith = Person('John','Smith')
richard_boettcher = Person('Richard','Boettcher')

print(john_smith.get_fname())
john_smith.set_fname('billy bob')
print(john_smith.get_fname())
john_smith.print_name()

# richard_boettcher.print_name()
# print("----------")
# # Person.last_name = "Craig"
# # Person.first_name = "Daniel"
# john_smith.print_name()
# print("----------")
# richard_boettcher.print_name()
# print("----------")
# richard_boettcher.first_name = "Richard"
# # richard_boettcher.last_name = "Boettcher"
# richard_boettcher.print_name()
# print("----------")
# Person.last_name = "dummy last"
# Person.first_name = "dummy first"
# john_smith.print_name()
# richard_boettcher.print_name()



# class Person():
#     """ A sample of what a class looks like."""
#     first_name = "James"
#     last_name = "Bond"

#     def print_name(self):
#         print(f"My name is {self.last_name}, {self.first_name} {self.last_name}")

