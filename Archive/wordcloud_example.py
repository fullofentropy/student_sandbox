import re
from wordcloud import WordCloud, STOPWORDS

def scan_file_text(regex_token=None, print_more=None, filename='romeo_and_juliet.txt'):
    file_to_open = filename

    wordlist = []

    print(type(wordlist))
    with open(file_to_open) as filehandler:
        if regex_token:

            # create a regex compile object
            compiled_regex_pattern = re.compile(regex_token)

            # lets look at each line and see what we can do with it using regex.
            for line in filehandler:
                tempwordlist = compiled_regex_pattern.findall(line.lower())
                [ wordlist.append(_.lower()) for _ in tempwordlist ]

        else:

            # lets look at each line and see what we can do with it not using regex.
            for line in filehandler:
                # strip out all words, make lower case
                tempList = line.strip().lower().split()
                [wordlist.append(_.strip(';-:\'\"!?.,[] ')) for _ in tempList]
                
    wordset = set(wordlist)

    if regex_token:
        print("RegEx search token used:",regex_token)
    print("There are -->",len(wordlist),"<-- words in",file_to_open)
    if print_more:
        print(wordlist)
    print("There are -->",len(wordset),"<-- unique words in",file_to_open)
    if print_more:
        print(wordset)
    return wordlist


def create_wordcloud(wordlist, output_name='output'):
    text = " ".join(wordlist)
    wordlist_len = len(wordlist)
    
    # STOPWORDS is a set of words imported from a file in the wordcloud package which will 
    # be eliminated from the wordcloud gui
    stopwords = set(STOPWORDS)
 
    # create wordcloud object
    wc = WordCloud(width = 1000, height = 1000,
                background_color ='white',
                stopwords = stopwords,
                min_font_size = 8)
 
    wc.generate(text)
 
    # save wordcloud
    wc.to_file("img/{}_count{}.png".format(output_name, wordlist_len))


# wordlist_no_regex = scan_file_text()
# create_wordcloud(wordlist_no_regex, 'no_regex')

"Richard's Car how is richard"
regex_pattern_list = [
    r'\w+',
    r'\w{6}',
    r'(\w+\W+){6}',
    r'([\w\']+\W+){6}',
    r'([\w\']+\w\W+){6}',
    r'\w{,5}',
    r'(\w+\W+){,5}',
    r'\w{5,6}\W',
    r'([\w\']{4,5}\w\W+)',
    r'([\w\']{4,}\w\W+)',
    r'\Wromeo\W|\Wjuliet\W|\Wlove\W',
    r'\W(romeo|juliet|love|hate)\W'
    r'\b\w{6}\b',
    r"\b[A-Za-z][A-Za-z']{4}[A-Za-z]\b",
    r'\b[A-Za-z\']{6}\b',
    r'\s(romeo|juliet|love|hate)\s',
    r'\b(romeo|juliet|love|hate)\b'
]

for _, pattern in enumerate(regex_pattern_list):
    wordlist = scan_file_text(pattern,print_more=None)
    create_wordcloud(wordlist,'wordcloud_'+str(_))


