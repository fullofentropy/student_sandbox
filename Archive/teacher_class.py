# Classes

# A class is code that specifies the data attributes and methods for a particular type of object

class Teacher(object):
    def __init__(self, name, college):
        self.name = name
        self.college = college

    def __str__(self):
        return f"{self.name} teaches at {self.college}"

    # def salary(self):
    #     print(f"{self.name} is paid $63,645.")
    def get_salary(self):
        print(f"{self.name} is paid $63,645.")


class PhysicsTeacher(Teacher):
    def __init__(self, name, college, specialty):
        Teacher.__init__(self, name, college)
        self.subject = "Physics"
        self.specialty = specialty

    def __str__(self):
        # return Teacher.__str__(self) + f", teaching the subject {self.subject} with a focus on {self.specialty}"
        return super().__str__() + f", teaching the subject {self.subject} with a focus on {self.specialty}"

    # def salary(self):
    #     salary = ""
    #     while not salary:
    #         salary = input(f"What is the salary for {self.name}?")
    #         try:
    #             self.salary = int(salary)
    #         except ValueError:
    #             print("That is not a valid value for the salary.  Please try again.")
    def set_salary(self):
        salary = ""
        while not salary:
            salary = input(f"What is the salary for {self.name}?")
            try:
                self.salary = int(salary)
            except ValueError:
                print("That is not a valid value for the salary.  Please try again.")

    def get_salary(self):
        return print(f"{self.name} is paid ${self.salary:,d}.")

# myPT = PhysicsTeacher("John",'UIUC','Quantum')
# myPT.salary()
# print(myPT.salary)
# myPT.salary()

t = Teacher('Smith',"uiuc")
pt = PhysicsTeacher('Adams','uiuc','quantum')
print(str(t))
print(str(pt))
pt.set_salary()
pt.get_salary()
t.get_salary()