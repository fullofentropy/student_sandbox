import codecs
import more_function


def encrypt_password(password):
    return codecs.encode(password, 'rot_13')

def decrypt_password(password):
    return codecs.encode(password, 'rot_13')

if __name__ == "__main__":
    print(encrypt_password("hunter2"))
    print(__name__)
    print(decrypt_password(encrypt_password("hunter2")))
    print(more_function.helloworld())
else:
    print(__name__)







