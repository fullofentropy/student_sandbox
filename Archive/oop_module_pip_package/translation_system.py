"""
https://pypi.org/project/mtranslate/

python -m pip install mtranslate
pip install mtranslate
pip3 install mtranslate
pip3.9 install mtranslate
py -3 -m pip install mtranslate
py -3.8 -m pip install mtranslate

"""

from mtranslate import translate as trans

test_data = """
现在我们要继续脚踏实地

电子商务的盈利在中国整体商业环境中简直是九牛一毛.

他无家可归，一无所有

她就是我的一见钟情
"""

print(trans(test_data))
