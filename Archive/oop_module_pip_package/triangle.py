divisor_count_to_find = 500

def triangle_number(n):
    return n*(n+1)/2

def divisors(tn):
    counter = 0
    n_sqrt = int(pow(tn, 0.5))
    for i in range(1, n_sqrt + 1):
        if tn%i == 0:
            counter += 2
    if n_sqrt * n_sqrt == tn:
        counter -= 1
    return counter

def start():
    start_number = 1
    div_numbers = 0
    while (div_numbers < divisor_count_to_find):
        tn = triangle_number(start_number)
        div_numbers = divisors(tn)
        start_number += 1
    print(div_numbers)
    print(tn)

if __name__ == '__main__':
    start()
    print(__name__)

# print(__name__)