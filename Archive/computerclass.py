class Processor():
    def __init__(self, name, speed, cores):
        self.name = name
        self.speed = speed
        self.cores = cores

class Motherboard():
    def __init__(self, name, ports):
        self.name = name
        self.ports = ports

class GraphicsCard():
    def __init__(self, name, memory, features):
        self.name = name
        self.memory = memory
        self.features = features

class Computer():
    def __init__(self, processor, motherboard, graphics):
        self.processor = processor
        self.motherboard = motherboard
        self.graphics = graphics

    def __str__(self):
        return f"""==========Computer information=========
        {self.processor.name}
        {self.processor.speed}
        {self.processor.cores}
        """


i9 = Processor("I9-11900KF", 5.3, 8)
i9_2nd = Processor("I9-happy", 4.4, 80)
rog_strix = Motherboard("ROG Strix Z590-E Gaming WiFi", ["HDMI", [8, "USB-A"], [2, "USB-C"]])
gf_3080 = GraphicsCard("MSI Gaming GeForce RTX 3080 OC Ventus", 1740, ["RTX", "DLSS"])
my_computer = Computer(i9, rog_strix, gf_3080)
the_middleman = Computer(i9, rog_strix, gf_3080)

print("-----------")
print(my_computer.processor.name)
print(the_middleman.processor.name)
print("-----------")
my_computer.processor = i9_2nd
my_computer.processor = 123
print(type(my_computer.processor))
# print(my_computer.processor.name)
# print(the_middleman.processor.name)
print(my_computer.graphics.memory)
print(gf_3080.memory)
# my_computer.graphics.memory = 123
# print(my_computer.graphics.memory)
# print(gf_3080.memory)
# gf_3080.memory = 666
# print(my_computer.graphics.memory)
# print(gf_3080.memory)
# the_middleman.graphics.memory = 555555
# print(my_computer.graphics.memory)
# print(gf_3080.memory)
# print(my_computer)