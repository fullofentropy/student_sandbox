# This program reads a file's contents into a list

def main():
    # Open a file for reading
    infile = open('cities.txt', 'r')

    cities_read_in = infile.readlines()

    # Close the file
    infile.close()

    # Strip the \n from each element
    count = 0
    while count < len(cities_read_in):
        cities_read_in[count] = cities_read_in[count].strip('\n ')
        print(cities_read_in[count])
        count += 1
    
    # for count in range(len(cities_read_in)):
    #     cities_read_in[count] = cities_read_in[count].strip('\n')

    # count = 0
    # while count < len(cities_read_in):
    #     print(cities_read_in[count])
    #     count += 1

    # Print the contents of the list
    print(cities_read_in)

# Call the main function
main()
