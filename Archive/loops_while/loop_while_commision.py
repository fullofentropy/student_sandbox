# This program calculates sales commission

def main():
    # Create a variable to control the loop
    keep_going = 'y'

    # Calculate a series of commissions
    while keep_going == 'y':
        # Get a salesperson's sales and commission rate.
        sale = float(input("Enter the amounts of sales: "))
        comm_rate = float(input("Enter the commission rate: "))

        # Calculate the commission
        commission = sale * comm_rate

        # Display the commission
        print('The commission is $', format(commission, ',.2f'), sep='')

        # See if the user wants todo another one
        keep_going = input("Do you want to calculate another " 
                           + 'commission (enter y for yes or' 
                           + 'another else for no)')

    else:
        print("Exiting the Program")


# Call the main
main()
