"""
Requirements:
Use a loop to display a table showing
the numbers 1 - 10 and their squares
"""

def main():
    #print the table headings
    print('Number\tSquare')
    print('--------------')

    #print the numbers 1 through 10 and their squares
    for num in range(1,11):
        square = num**2 # raise the first number to power of second number/exponentiation
        print(num, '\t', square)

main()