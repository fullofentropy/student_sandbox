"""
Requirements:
Print out shapes using for loops
"""
# """Example 1:   single line shape """
# for col in range(6):
#     # print('*', end='')
#     # print('*')

"""Example: 2  print a shape of 8 rows and 6 columns """

# for row in range(8):
#     for col in range(6):
#         print('*', end='')
#     print()

"""Example 3:  print a pattern with user defined info """

# #get rows and columns from user
# rows = int(input('Enter rows: '))
# columns = int(input('Enter cols: '))

# #display the pattern
# for row in range(rows):
#     for col in range(columns):
#         print('*', end='')
#     print()

"""Example 4:  Display a star-step pattern """
NUM_STEPS = 6

for r in range(NUM_STEPS):
    for c in range(r):
        # print(' ', end='')
        print(c, end='')
    print('#')