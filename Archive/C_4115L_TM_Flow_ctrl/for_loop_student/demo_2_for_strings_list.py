"""
Requirements:
Demonstrate a simple for loop that uses a list of strings

"""

def main():

    for name in ['Hulk', 'Ironman', 'Widow']:
        print(name)

main()