
# Q - Define what iteration means?
# A - ?
   
# Q - Define accumulator?
# A - ?

# Q = The range function takes three arguments, what are they
# A - ?
# Q - In the for loop, for num in range(10): what is 'num'?
# A - ?

# Q - When iterating over a dictionary, what is a function that you can use to iterate over the dictionary ?
# A - ?

# Q - What does the python operator ** do.  example num**2
# A - ?

# Q - for this code:
    for x in range(5):
        print(x)
# What will the output be
# A - ?

# Q - for the code:
 for x in range(5):
        print(x)
# what can be added in the print statement to make it so that 
#all the numbers print on the same line?
# A - ?

