
with open('fib.txt','w') as fh:
    n1 = 0
    n2 = 1
    count = 0
    print("Fibonacci sequence:")
    while count < 100:
        print(n1)
        fh.write(f"{count}--->" + str(n1) + "<---\n")
        nth = n1 + n2 
        n1 = n2
        n2 = nth
        count += 1