# This program calculates sales commission

def main():
    # Create a variable to control the loop
    keep_going = 'y'

    # Calculate a series of commissions
    while keep_going == 'y':
        # Call the show_commission function to display the commissions
        show_commission()
        # See if the user wants to do another one
        keep_going = input("Do you want to calculate another commission"
                          + " (enter y for yes or anthing else for no)")

def show_commission():
        # Get a salesperson's sales and commission rate
        sales = float(input("Enter the amounts of sales: "))
        comm_rate = float(input("Enter the commission rate: "))

        # Calculate the commission
        commission = sales * comm_rate/100

        # Display the commission
        print( 'The commission is $', format(commission, ',.2f'), sep='')


# Call the main
main()
