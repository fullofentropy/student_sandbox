def bugCollector():
    bugs = {}
    total = 0
    days = 7
    
    for i in range(1,days + 1):
        valid_input = False
        while not valid_input:
            try:
                countbugs = int(input(f"How many bugs did you collect on day {i}: "))
                total += countbugs
                bugs[f'Day{i}']=countbugs

                valid_input = True
            except:
                print("Enter a number not a character")
                #bugCollector()
    print(total)
    print(bugs)