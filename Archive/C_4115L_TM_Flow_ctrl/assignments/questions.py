def program():
    amount = 33 
    def sales_tax(amount2):
        rate = 0.0625
        tax_total = amount * rate # Amount can be seen here
        total = tax_total + amount
        print(total)

    sales_tax(200) # Amount could be seen here
    
program() # Amount could not be seen here