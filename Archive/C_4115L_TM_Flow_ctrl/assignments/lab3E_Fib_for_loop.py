"""
Instructions
Write a file that prints out the first 100 numbers in the Fibonacci sequence iteratively
Reference for further information on Fibonacci
"""

N = 100

#initialize with starting elements: 0, 1
fibonacciSeries = [0,1]

if N>2:
    for i in range(2, N):
        #n ext element in series = sum of its previous two numbers
        nextElement = fibonacciSeries[i-1] + fibonacciSeries[i-2]
        # append the element to the series
        fibonacciSeries.append(nextElement)

print(fibonacciSeries)