# This program reads a file's contents into a list

def main():
    with open('cities.txt', 'r') as fh:

        # read the contents of th efile into a list
        cities_read_in = fh.readlines()

    # Strip the \n from each element
    index = 0
    while index < len(cities_read_in):
        cities_read_in[index] = cities_read_in[index].rstrip('\n')
        index += 1

    # Print the content of the list
    print(cities_read_in)

# Call the main function
main()
