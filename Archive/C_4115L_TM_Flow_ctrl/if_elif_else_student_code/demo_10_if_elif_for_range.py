for a in range(51):
  if a == 0:
    print('{} you cannot divide by zero!'.format(a))
  elif a % 10 == 0:
    print('{} can be divided by 10!!'.format(a))
  elif a % 2 == 0:
    print('{} is even!'.format(a))
  else:
    print('{} is odd!'.format(a))