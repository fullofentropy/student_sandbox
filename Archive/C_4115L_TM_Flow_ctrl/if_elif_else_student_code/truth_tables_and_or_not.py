#   Truth Table for 'and':  only true if both are true
#   true and false:  False
#   false and true:  
#   false and false: 
#   true and true:   

#   Truth Table for 'or':  only false if both are false
#   true or false: 
#   false or true: 
#   false or false: 
#   true or true: 
  
#   Truth Table for 'not':
#   not true: 
#   not false: 