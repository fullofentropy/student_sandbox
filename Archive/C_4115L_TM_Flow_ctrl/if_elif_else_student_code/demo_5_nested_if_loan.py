"""
Requirements:
This function is going to determine if a bank customer qualifies for a loan
based on salary and years on the job.  They must have a minimum salary of 30,000 
and have been employed a minimum of 2 years
if they meet all the qualifications display 'Your qualify for the loan!'
if they do not meet the qualifications display  the qualifications
# ie. 'You must have been employed for at least 2 years to qualify'

"""
#global constangs
MIN_SALARY = 30000.0
MIN_YEARS = 2

def main():

    #get customer's annual salary
    salary = float(input('Enter your annual salary: '))

    #get the number of years on the current job
    years_on_job = int(input('Enter the number of years employed: '))

    #if customer salary is greater than or equal to the min salary continue
    if salary >= MIN_SALARY:
        if years_on_job >= MIN_YEARS:
            print("You qualify for the loan!")

        else:
            print("you must been employed for at least ", MIN_YEARS, "years to qualify")
    else:
        print('You must earn at least $', format(MIN_SALARY, ',.2f'), ' per year to qualify.')
main()