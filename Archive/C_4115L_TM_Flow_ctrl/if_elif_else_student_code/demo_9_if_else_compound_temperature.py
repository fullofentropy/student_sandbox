""" !!!!!!!!!!!!! evaluating numberic ranges with compound expressions   !!!!!!!!!!!!!!!!!!!"""
y = -2
x = 60
input_value = float(input("Provide a temp in degrees F: "))
if x>= input_value >= y: 
# if input_value >= y and input_value <= x:
    print('The temperatrue is in an acceptable range')
else:
    print('The temperatrue is not in an acceptable range')

# #we could also change this to look like

# if ********* :#second astrik
#   print('The temperature is in an acceptable range')
# # If we want to notify that the temperature is not within 
# # the range we add an else statement
# else:
#    print('The temperature is not in an acceptable range')

# # we can do this with an or as well

# if ***********: $third astrik
#   print('The temperature is in an acceptable range')


# # and if we change the value of x to -2
# # x = -2

# # it will evaluate to false because both are false
# # and it will run the else