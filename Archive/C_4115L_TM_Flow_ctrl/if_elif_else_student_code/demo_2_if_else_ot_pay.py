"""
Requirements:
This program takes hours worked and pay rate and checks if there is any overtime pay to calculate
The main function gets the number of hours worked and the hourly
pay rate.
It calls either the calc_pay_with_OT function
or 
the calc_regular_pay function
to calculate and display the gross pay
"""

#global constants
BASE_HOURS = 40
OT_MULTIPLIER = 1.5

def main():

#get the hours worked and the hourly pay rate from user
   hours_worked = float(input("Enter the number of hours worked: "))
   pay_rate = float(input("Enter the hourly pay rate: "))

   #calculate and display the gross pay
   if hours_worked > BASE_HOURS:
      calc_pay_with_OT(hours_worked, pay_rate)
   # elif hours_worked <= BASE_HOURS:
   else:
      calc_regular_pay(hours_worked, pay_rate)
   
   #The calc_pay_with_OT function calculates pay with overtime.
   # It acceptes the hours worked and the hourly pay rate
   # the gross pay is displayed

def calc_pay_with_OT(hours, rate):
   #calculate the amount of over time worked
   print("There is Overtime pay. ")
   overtime_hours = hours - BASE_HOURS

   #calculate the overtime pay
   overtime_pay = overtime_hours * rate * OT_MULTIPLIER

   #calculate the gross pay
   gross_pay = BASE_HOURS * rate + overtime_pay

   print('The gross pay is $', format(gross_pay, ',.2f'), sep = "")


# The calc_regular_pay function calculates pay with no overtime.  
# It accepts the hourly pay rate.  
# The gross pay is displayed
def calc_regular_pay(hours, rate):
   print("There is not overtime pay. ")
   gross_pay = hours * rate
   print('The gross pay is $', format(gross_pay, ',.2f'), sep="")


# call main to run the program
main()