"""
Requirements:
This program gets a numeric test score from the user and displays the corresponding letter grade
A = 90
B = 80
C = 70
D = 60
"""
   
#grade global constants
A_SCORE = 90
B_SCORE = 80
C_SCORE = 70
D_SCORE = 60

def main():

   score = int(input('Enter your test scroe: '))

   #determine the grade
   if score >= A_SCORE:
      print('Your got an A')
   else:
      if score >= B_SCORE:
         print('Your got an B')
      else:
         if score >= C_SCORE:
            print('Your got an C')
         else:
            if score >= D_SCORE:
               print("you got an D")
            else:
               print("you got an F")
   # if score >= A_SCORE:
   #    print('Your got an A')
   # elif score >= B_SCORE:
   #    print('Your got an B')
   # elif score >= C_SCORE:
   #    print('Your got an C')
   # elif score >= D_SCORE:
   #    print("you got an D")


#call main
main()