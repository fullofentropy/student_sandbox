# Q - What does the if statement check for within a condtion?
# A - ?

# Q - Will a condition run if a statement in an if is false?
# A - ?

# Q = What statement can you  use if you wish to check for more 
# than one condtion with an if statement?
# A - ?

# Q - what is an else statement used for if no previous statements
# evaluate to true?
# A - ?

# Q - For 'and' logical operators truth table,
# when is the only time a statement will evaluate to true?
# A - ?

# Q - For 'or' logical operators truth table,
# when is the only time a statement will evaluate to false?
# A - ?

# Q - For 'not' logical operators truth table,
# what happens with the statement not true:
# A - will evaluate to false

# Q - For 'not' logical operators truth table,
# what happens with the statement not false:
# A - ?


# Q - If you have a piece of code written like this, what is the output and why?
   num = 13

  if(num = 3):
    print('your numnber is equal to our number')
  else:
    printf('your number is not equal to our number')



# A - ?


# Q - If you have a piece of code written like this, what is the output and why?
  num = 7.5 % 3 == 0
  if num:
    print("your number is ", num) # true
  else: 
    print("your number is ", num) #false


# A - ?

# Q - If you have a piece of code written like this, what is the output and why?
first_names = ['Joe', 'Christina', 'Richard']

if 'joe' in first_names:
   print("That name exists")
else:
   print("That name does not exist")

# A - ?

# Q - If you have a piece of code written like this, what would cause a syntax error?

if num == 20
  print("The number is equal to 20")

# A - ?

# Q - How many spaces does PEP8 recommend for indenting?
# A - ?
