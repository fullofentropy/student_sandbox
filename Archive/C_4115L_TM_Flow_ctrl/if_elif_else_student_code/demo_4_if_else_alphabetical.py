"""
Requirments:
This program compares two strings with the < operator 
and then retruns the strings in alphabetical order
"""


def main():

    # get names from the user
    name1 = input('Enter a name(last name first): ')
    name2 = input('Enter a name(last name first): ')

    print('These are the names listed alphabetically: ')

    if name1 < name2:
        print(name1,'\n',name2)
    else:
        print(name2, '\n',name1)

main()