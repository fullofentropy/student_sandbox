"""
Requirements:
 This program will take in a string from the user and compare it
 with the accepted password of 'SuperSecretPassword!' and display
'Passowrd accepted' if
the passwords match
and 'Password is wrong, sorry' if it does not match
"""

def main():

    #get password from the user
    password = input('Enter the password: ')

    #Determine whether or not the password was correct
    if password == 'SuperSecretPassword!':
        print("Password accepted")
    else:
        print('Password is wrong, sorry')
      

main()