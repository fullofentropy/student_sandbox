"""
Requirements:
Create a program that gets three test scores and displays their average.
Congratulate the user if their average is equal to or greater than 95
"""
#global constant
HIGH_SCORE = 95

# #get test scores
test1 = int(input('Enter the score for test 1: '))
test2 = int(input('Enter the score for test 2: '))
test3 = int(input('Enter the score for test 3: '))

#calculate the average of the test scores

average = (test1 + test2 + test3)/3


print('The average score is {:.2f}'.format(average))

#processing
if average >= HIGH_SCORE:
    print(f"Congradulations your score was greater than {HIGH_SCORE}")

