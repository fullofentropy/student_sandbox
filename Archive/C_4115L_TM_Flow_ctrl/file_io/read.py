# This program reads and isplays the contents
# of the philosophers.txt file

def main():
    # Open a file named philosophers.txt
    infile = open('philosophers.txt', 'r')

    # Read the files contents
    file_contents = infile.read()

    infile.close()

    # print the data from the file
    print(file_contents)

# Call the main fucntion
main()