# This program reads all of the values in the sales.txt file

def main():
    # Open the sales.txt file for reading
    sales_file = open('sales.txt', 'r')

    # Read the first line from the file, but we are not going to
    # convert it to a number yet
    # We need to test for an empty string

    line = sales_file.readline()

    while line != '':
        # convert the line to a float
        amount = float(line)

        # format and display the amount
        print(format(amount, ',.2f'))

        # Read the next line
        line = sales_file.readline()

    # close the file
    sales_file.close()

# call the main function
main()