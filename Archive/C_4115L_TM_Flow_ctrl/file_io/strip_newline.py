# This program reads the contents of philosophers.txt
# file one line at a time and then strips the newline char
# from the right side

def main():
    # open philosophers.txt for reading
    infile = open('philosophers.txt', 'r')

    # read the lines one by one from the file
    line1 = infile.readline()
    line2 = infile.readline()
    line3 = infile.readline()

    # Strip the \n from each string
    line1 = line1.rstrip('\n')
    line2 = line2.rstrip('\n')
    line3 = line3.rstrip('\n')

    infile.close()

    print(line1, end="____")
    print(line2, end="__dd__")
    print(line3, end="_ee5___")

# Call the main function
main()