# This program tries to display the contents of a file
# that the user specifies

def main():
    # Get the name of the file
    filename = input('Enter the file name: ')
    try:
        # Open the file
        with open(filename, 'r') as infile:
            # Read the files contents
            contents = infile.read()
            print(contents)

    except FileNotFoundError:
        print(f"The file {filename} can not be found.")

    finally:
        print("I love python")
    # Display the files contents
    # finally:

# Call the main function
main()


