# this program demo's how numbers that are read from a file must be
# converted from strings befor they are used in math operations.

def main():
    # Open a file for reading
    infile = open('numbers.txt', 'r')

    # Read three numbers from the file
    num1 = int(infile.readline())
    num2 = int(infile.readline())
    num3 = int(infile.readline())

    # Close the file
    infile.close()

    # Add the three numbers
    total = num1 + num2 + num3
    # display the numbers and their total
    print(f'The numers are {num1}, {num2}, {num3}')
    print(f'The numbers are {num1 : #b}, {num2 : #x}, {num3 : #o}')
    print(f'The total is: {total}')

# Call the main function
main()
