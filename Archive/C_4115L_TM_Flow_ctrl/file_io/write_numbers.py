# this program demonstrates how numbers must be converted
# to strings before they are written to a text file

def main():
    # Open a file for writing
    outfile = open('numbers.txt', 'w')

    # Get thre numbers from the user
    num1 = int(input("Enter a number: "))
    num2 = int(input("Enter a number: "))
    num3 = int(input("Enter a number: "))


    outfile.write(str(num1) + '\n')
    outfile.write(str(num2) + '\n')
    outfile.write(str(num3) + '\n')

    # Close the file
    outfile.close()
    print("Data was written to numbers.txt")

# call the main function
main()