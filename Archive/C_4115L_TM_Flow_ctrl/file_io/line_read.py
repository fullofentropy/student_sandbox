# This program reads the contents of philosophers.txt
# file one line at a time

def main():
    # open philosophers.txt for reading
    infile = open('philosophers.txt', 'r')

    # read the lines one by one from the file
    line1 = infile.readline()
    # line2 = infile.readline()
    # line3 = infile.readline()
    # myList=[]
    # for line in infile.readlines():
    #     myList.append(line)

    infile.close()

    print(line1)
main()