"""
When a program need to save data for later use, it writes the data in a file
The data can be read from the file at a later time.

'r' - open a file for reading
'w' - opens a file for writing
'a' - opens a file to be written, appending at the end
'+' - makes the mode both raed and write or (write and read)
'b' - binary mode

"""

def main():
    # Open file name philosophers.txt
    """
    This function will write text to a file called philosophers.txt
    """
    outfile = open('philosophers.txt', 'w')

    # write the anems of three philosophers.
    outfile.write("Seneca\n")
    outfile.write("Socrates\n")
    outfile.write("Nicolas Flanel\n")

    outfile.close()

# call the main function
main()