# This program uses the for loop to read 
# all of the values in the sales.txt

def main():
    # Open the sales.txt file for reading
    sales_file = open('sales.txt', 'r')

    # read all of the lines from the file
    for line in sales_file:
        # conver the line to a float
        amount = float(line)
        # Format and display the amount
        print(format(amount, ',.2f'))

    # close the file
    sales_file.close()

# Call the main function
main()