# Q - Define what iteration means?
# A - 
    # -Iteration terminates when the loop-continuation condition fails
    # -When a loop repeatedly executes until the controlling condition becomes false

# Q - Define accumulator?
# A - A variable that changes with every iteration of a structure

# Q = The range function takes three arguments, what are they
# A - start, stop, step

# Q - In the for loop, for num in range(10): what is 'num'?
# A - Num is an iterator

# Q - When iterating over a dictionary, what is a function that you can use to iterate over the dictionary ?
# A - items()

# Q - What does the python operator ** do.  example num**2
# A - raises the number on the left to the power of the number on the right

# Q - for this code:
    for x in range(5):
        print(x)
# What will the output be
# A - 0, 1, 2, 3, 4 (all on seperate lines)

# Q - for the code:
 for x in range(5):
        print(x)
# what can be added in the print statement to make it so that 
#all the numbers print on the same line?
# A - print(x, end='')


