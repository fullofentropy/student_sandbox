"""
Requirements:
Demonstrate using a for loop to convert mph to kph and increment by 10
Display in a table

"""
#global constants
START = 10
END = 120
INCREMENT = 10
CONVERSION_FACTOR = 0.6214

def main():

    #print the table headings
    print('KPH\tMPH')
    print('--------')
    kph = 0
    #print the speeds
    for kph in range(START,END+1,INCREMENT):
        mph = kph * CONVERSION_FACTOR
        print(kph, '\t', format(mph, '.1f'))

main()