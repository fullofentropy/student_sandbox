"""
Requirements:
Demonstrate a simple for loop that uses range to list numbers
"""

def main():

    print('I will display the numbers 1 through 5')
    for num in range(1,6):
        print(num)

main()