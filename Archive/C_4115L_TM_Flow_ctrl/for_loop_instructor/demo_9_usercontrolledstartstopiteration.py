"""
Requirements:
Demonstrate a user controlled start and stop for loop that prints out a table of squares
"""

from turtle import st


def main():
    print()
    print('(starting and stopping where the user determines) and their squares.')

    start_square = int(input('Where should I start?: '))
    last_square = int(input('How high should I go?: '))

    #Print Table headings
    print()
    print('Number\tSquare')

    #print the numbers and their squares
    for num in range(start_square,last_square+1):
        square = num**2
        print(num, '\t', square)


main()