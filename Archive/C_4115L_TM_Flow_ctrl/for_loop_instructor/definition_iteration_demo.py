#md book working example


""" Example 1: for loop in c language """
# for(int i = 0; i < strlen(my_string); i++)
# {
#      printf("%c\n", my_string[i])
# }  
   

# """Example 2:  python in c style for loop """
# my_string = "Python"
# for i in range(len(my_string)):
#     print(my_string[i])



""" Example 3: pythonic for loop """
my_string = "Python"
for letter in my_string:
    # print(letter)
    print(letter, end='')

"""
the 'in' operator calculates the count of my_string
it iterates through it as the variable letter
the value of letter is my_string[i](aka my_string[letter])

"""
#example of what is going on with 'in' 
# print(my_string[0]) #my_string[letter]
# print(my_string[1]) #my_string[letter]
# print(my_string[2]) #my_string[letter]
# print(my_string[3]) #my_string[letter]
# print(my_string[4]) #my_string[letter]
# print(my_string[5]) #my_string[letter]

# """ Example 4: looping dictionaires with a for loop Python 2 """
# #Python 2 use the method iteritems()
# for key, value in dict.iteritems():
#     pass

# """ Example 5:  looping dictionaries with a for loop Python 3 """
# # Python 3 use the method items()
# for key, value in dict.items():
#     pass

# """Example 6:  looping dictionary python 3 example """
# x = {'stu1': 'James', 'stu2': 'Bob', 'stu3': 'Rengar'}

# for stu_id, name in x.items():
#     print("{}'s name is {}".format(stu_id, name))

"""  DEFINITIONS """
"""
*** Iteration and Recursion each involve a termination test ***

Iteration:
    -Iteration terminates when the loop-continuation condition fails
    -When a loop repeatedly executes until the controlling condition becomes false
Recursion:
    -When a statement in a function calls itself repeatedly
    -Recursion terminates when a base case is recognized

"""