"""
Requirements:
Demonstrate a user controlled for loop that prints out a table of squares

"""

def main():
    print()
    print('Starting at 1 and their squares.')

    try:
        last_square = int(input('How high should I go? '))
    except ValueError:
        print("Please enter an integer value:")
        main()

    print()
    print('Number\tSquare')

    #Print the numbers and their squares
    for num in range(1,last_square):
        square = num**2
        print(num, '\t', square)
 
main()