"""
Requirements:
Demonstrate a for loop that lists the numbers 1 through ten but only every other one
"""

def main():

    print('I will display the odd numbers from 1 to 10')
    for num in range(1, 10, 2):
        print(num)

main()