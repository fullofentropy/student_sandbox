"""
Requirements:
This program calculates the sum of a series of numbers entered
"""
MAX = 5

def main():

    #must initialize an accumulator
    total = 0.0

    print('This program calculates the sum of' )
    print(MAX, 'numbers you will enter.')
    print()

    #get the numbers and accumulat them
    for counter in range(MAX):
        number = int(input('Enter a number: '))
        total = total + number

        #or write total this way
        # total += number
    
    #display the total of the numbers
    print('The total is', total)


main()