"""
Requirements:
Demonstrate the range function with a for loop

"""

def main():

    #print a message 5 times
    for x in range(1,6):
        print('Hello World!', x)

main()