"""
Requirments:
Get the number of students and the number of test scores 
use a nested for loop to get the averages and display average

"""

def main():
    
    #get number of students
    num_students = int(input('How many students do you have?: '))

    #get number of test scores
    num_test_scores = int(input('How many test scores per student?: '))

    #determine each students average score
    for student in range(num_students):
        #initialize an accumulator for test scores
        totals = 0.0
        #get a student's test corese
        print('Student number', student + 1)
        print('-------------------')
        for test_num in range(num_test_scores):
            print('Test number', test_num + 1,sep="@", end='')
            score = float(input(': '))
            #add the score to the accumulator
            totals += score
        #calculate the average test score for this student
        average = totals / num_test_scores

        #display the average
        print('The average for student number', student + 1, 'is: ', average)
        print()

main()