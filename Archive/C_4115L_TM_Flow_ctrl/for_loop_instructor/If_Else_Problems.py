"""
1. Roman Numerals
Write a program that prompts the user to enter a number within the range of 1 through 10.
The program should display the Roman numeral version of that number. If the number is
outside the range of 1 through 10, the program should display an error message.

2. Areas of Rectangles
The area of a rectangle is the rectangle’s length times its width. Write a program that asks
for the length and width of two rectangles. The program should tell the user which rectangle
has the greater area, or if the areas are the same.

3. Mass and Weight
Scientists measure an object’s mass in kilograms and its weight in newtons. If you know the
amount of mass of an object in kilograms, you can calculate its weight in newtons with the
following formula:

    weight = mass * 9.8

Write a program that asks the user to enter an object’s mass, and then calculates its weight.
If the object weighs more than 1,000 newtons, display a message indicating that it is too
heavy. If the object weighs less than 10 newtons, display a message indicating that it is too
light.

4. Magic Dates
The date June 10, 1960, is special because when it is written in the following format, the
month times the day equals the year:
    6/10/60
Design a program that asks the user to enter a month (in numeric form), a day, and a two digit year. The program should
then determine whether the month times the day equals the year. If so, it should display a message saying the date is magic.
Otherwise, it should display a message saying the date is not magic.

5. Color Mixer
The colors red, blue, and yellow are known as the primary colors because they cannot be
made by mixing other colors. When you mix two primary colors, you get a secondary color,
as shown here:

    When you mix red and blue, you get purple.
    When you mix red and yellow, you get orange.
    When you mix blue and yellow, you get green.

Design a program that prompts the user to enter the names of two primary colors to mix.
If the user enters anything other than “red,” “blue,” or “yellow,” the program should display an error message. Otherwise,
the program should display the name of the secondary
color that results.

6. Change for a Dollar Game
Create a change-counting game that gets the user to enter the number of coins required to make
exactly one dollar. The program should prompt the user to enter the number of pennies,
nickels, dimes, and quarters. If the total value of the coins entered is equal to one dollar, the program should congratulate
the user for winning the game. Otherwise, the program should display a message indicating whether the amount entered was
more than or less than one dollar

7. Book Club Points
Serendipity Booksellers has a book club that awards points to its customers based on the
number of books purchased each month. The points are awarded as follows:
• If a customer purchases 0 books, he or she earns 0 points.
• If a customer purchases 1 book, he or she earns 5 points.
• If a customer purchases 2 books, he or she earns 15d points.
• If a customer purchases 3 books, he or she earns 30 points.
• If a customer purchases 4 or more books, he or she earns 60 points.
Write a program that asks the user to enter the number of books that he or she has purchased this month and displays the
number of points awarded.

8. Software Sales
A software company sells a package that retails for $99. Quantity discounts are given
according to the following table:

    Quantity        Discount
    10–19           20%
    20–49           30%
    50–99           40%
    100 or more     50%

Write a program that asks the user to enter the number of packages purchased. The program should then display the amount
of the discount (if any) and the total amount of the
purchase after the discount.

9. Shipping Charges
The Fast Freight Shipping Company charges the following rates:

    Weight of Package                               Rate per Pound
    2 pounds or less                                $1.10 
    Over 2 pounds but not more than 6 pounds        $2.20
    Over 6 pounds but not more than 10 pounds       $3.70
    Over 10 pounds                                  $3.80

Write a program that asks the user to enter the weight of a package and then displays the
shipping charges.

10. Body Mass Index Program Enhancement
In programming Exercise #6 in Chapter 3 you were asked to write a program that calculates a person’s body mass index
(BMI). Recall from that exercise that the BMI is often used
to determine whether a person is overweight or underweight for their height. A person’s
BMI is calculated with the formula

    BMI = weight * 703 / height^2

where weight is measured in pounds and height is measured in inches. Enhance the program so it displays a message
indicating whether the person has optimal weight, is
underweight, or is overweight. A person’s weight is considered to be optimal if his or
her BMI is between 18.5 and 25. If the BMI is less than 18.5, the person is considered
to be underweight. If the BMI value is greater than 25, the person is considered to be
overweight.

11. Time Calculator
Write a program that asks the user to enter a number of seconds, and works as follows:
• There are 60 seconds in a minute. If the number of seconds entered by the user is greater
than or equal to 60, the program should display the number of minutes in that many
seconds.
• There are 3,600 seconds in an hour. If the number of seconds entered by the user is
greater than or equal to 3,600, the program should display the number of hours in that
many seconds.
• There are 86,400 seconds in a day. If the number of seconds entered by the user is greater
than or equal to 86,400, the program should display the number of days in that many
seconds.

"""
