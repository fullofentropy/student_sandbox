"""
Requirements:
Demonstrate a simple for loop that uses a list of strings

"""

def main():

    for name in ['bill','bob','jane']:
        print(name)

main()