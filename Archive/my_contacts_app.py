"""
(Name and Email Addresses)
Write a program that keeps names and email addresses in a dictionary as key-value pairs.
The program should display a menu that lets the user look up a person’s email address, add
a new name and email address, change an existing email address, and delete an existing
name and email address. The program should pickle the dictionary and save it to a file
when the user exits the program. Each time the program starts, it should retrieve the dictionary 
from the file and unpickle it.

"""
import pickle
import json

LOOK_UP = 1
ADD = 2
CHANGE = 3
DELETE = 4
SAVE = 5
LOAD  =6
QUIT = 7

DATA_JSON = True
DATA_PICKLE = None

def main():
    if DATA_PICKLE:
        filename = 'my_contacts.dat'
    elif DATA_JSON:
        filename = 'my_contacts2.json'

    contact_list = {}
    choice = 0
    file_loaded = None

    while choice != QUIT:
        # get the user's menu choice
        choice = get_menu_choice()

        # Process the choice
        if choice == LOOK_UP:
            look_up(contact_list)

        elif choice == ADD:
            add(contact_list)

        elif choice == CHANGE:
            change(contact_list)

        elif choice == DELETE:
            delete_contact(contact_list)
        
        elif choice == LOAD:
            try:
                if DATA_PICKLE:
                    contact_list = load_contact_pickle(filename)
                    file_loaded = True
                elif DATA_JSON:
                    contact_list = load_contact_json(filename)
                    file_loaded = True

            except FileNotFoundError:
                print(f'{filename} was not found, creating one with that name.')
                contact_list = {}
                if DATA_PICKLE:
                    save_contact_pickle(contact_list, filename)
                elif DATA_JSON:
                    save_contact_json(contact_list, filename)

        elif choice == SAVE:
            answer = ""
            if not file_loaded:
                answer = input("WARNING: the system has not loaded any files yet, if a file exists,"
                       + " performing this action will overwrite that file.\nDO YOU WANT TO CONTINUE (y/n): ")
                if answer.lower() == 'y':
                    if DATA_PICKLE:
                        save_contact_pickle(contact_list, filename)    
                    if DATA_JSON:
                        save_contact_json(contact_list, filename)    
            else:
                if DATA_PICKLE:
                    save_contact_pickle(contact_list, filename)
                if DATA_JSON:
                    save_contact_json(contact_list, filename)

def get_menu_choice():
    """
    this is the User Interface for this program.
    """
    print()
    print('Welcome to My Contact email List program')
    print('--------------------')
    print("""
        1. Look up a contact
        2. Add a new contact
        3. Change a contact
        4. Delete a contact
        5. Save Contact list
        6. Load Contact list
        7. Quit the program
        """)

        # Get the user's choice
    choice = int(input('enter your choice: '))
    
    #Validate choice
    while choice < LOOK_UP or choice > QUIT:
        choice = int(input('Enter a valid choice: '))
    return choice

def look_up(contact_list):
    # Get a name to look up
    name = input("Enter a name: ")

    # look it up in the dict
    print(contact_list.get(name, "Name not found"))
    


# The add function adds a new entry into the contacts dictionary
def add(contact_list):
    # get a name and a birthday
    name = input("Enter the name you want to add: ")
    email = input('Enter the email: ')

    # If the name does not exist , add it
    if name not in contact_list:
        contact_list[name] = email
    else:
        print('That entry already exists.')

# The change method changes an existing entry in teh email dict
def change(contact_list):
    #get a name to look up
    name = input("Enter the name you want to change: ")
    if name in contact_list:
        # Get a new email
        email = input('Enter their new email: ')
        #update the entry
        contact_list[name] = email
    else:
        print("Could not find the name you requested. ")

# the delete function delets an etnry from the contact_list dictionary
def delete_contact(contact_list):
    # get a name to delete
    name = input("Enter the name: ")

    # if the name exists, delet the entry
    if name in contact_list:
        del contact_list[name]
    else:
        print('That name is not in the dictionary')

def load_contact_pickle(filename_string):
    """
    this contact list will load a pickle file if it exists.
    """
    
    with open(filename_string, 'rb') as file:
        return pickle.load(file)

def save_contact_pickle(data, filename_string):
    """
    this will save a pickle file
    """
    with open(filename_string, 'wb+') as file:
        return pickle.dump(data, file)

def load_contact_json(filename_string):
    """
    this contact list will load a pickle file if it exists.
    """
    
    with open(filename_string, 'r') as file:
        return json.load(file)

def save_contact_json(data, filename_string):
    """
    this will save a pickle file
    """
    with open(filename_string, 'w+') as file:
        return json.dump(data, file)

main()