"""
Checks a certain range of numbers to see if they can divide into a user specified num
"""
# Program main, runs at start of program
def launch():
    num = input('What number would you like to check?-->')
    amount = input('How many numbers do you want to check?-->')

    if isInt(num) == False or isInt(amount) == False:
        print("You must enter an integer..")
        launch() 
    elif int(amount) < 0 or int(num) < 0:
        print("You must enter a number greater than 0")
        launch() 
    else:
        divisible_by(int(num), int(amount))

# Checks num divisible numbers up to amount or itself
def divisible_by(num, amount):
    i = 1.0
    while (num / i >= 1 and amount > 0):
        if num % i == 0:
            print('{} is divisible by {}'.format(int(num), int(i)))
            amount -= 1
        i += 1

# EXCEPTION HANDLING
def isInt(x):
    try:
        int(x) ###
        return True
    except ValueError as greatness:
        # Q: How might I be able to get the actual error printed to console? 
        print(greatness)
        return False

# launch()
# ------------------2ND EXAMPLE IS BELOW HERE-------------------

class CustomError(Exception):
    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "your error is {}".format(self.msg)

class CustomError2(Exception):
    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "your error is {}".format(self.msg)

class CustomError3(Exception):
    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "your error is {}".format(self.msg)


def doStuff(danger):
    if danger == True:
        raise CustomError("Whoa don't do that!")
    else:
        print("Success")

def doStuff2(danger):
    if danger == True:
        raise CustomError2("Whoa don't do that! I'm Two")
    else:
        print("Success2") 

def doStuff3(danger):
    if danger == True:
        raise CustomError3("Whoa don't do that! I'm Three")
    else:
        print("Success3") 


def main(stuffer=False, stuffer2=False, stuffer3=False):
    try:
        doStuff(stuffer)
        doStuff2(stuffer2)
        doStuff3(stuffer3)
        
    except CustomError as stuff:
        print(stuff)

    except CustomError2 as stuff:
        print(stuff)

    except CustomError3 as stuff:
        print(stuff)

    else:
        print("I am else printing")

    finally:
        print("I am finally printing")

print("-True, True, True--Trying With Customer Exception Error using Exception Class---")
main(True, True, True)
print()
print("-False, True, True--Trying With Customer Exception Error using Exception Class---")
main(False, True, True)
print()
print("-True, False, True--Trying With Customer Exception Error using Exception Class---")
main(True, False, True)
print()
print("-False, False, True--Trying With Customer Exception Error using Exception Class---")
main(False, False, True)
print()

print("---Trying When no Exception occures---")
main(False, False, False)