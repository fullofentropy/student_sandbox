import random

def main_question_1():
    question = """

    1. File Display
    Create a file containing a series of integers named numbers.txt. 
    Write a program that displays all of the numbers in the file. 
    """
    print(question)
    create_numbers_file()
    display_numbers()


def main_question_2():
    question = """

    2. File Head Display
    Write a program that asks the user for the name of a file. 
    The program should display only the first five lines of the 
    file’s contents. If the file contains less than five lines, 
    it should display the file’s entire contents.
    """
    print(question)
    display_first_five_lines_of_file()


def main_question_3():
    question = """

    3. Line Numbers
    Write a program that asks the user for the name of a file. 
    The program should display the contents of the file with each 
    line preceded with a line number followed by a colon. The
    line numbering should start at 1.
    """
    print(question)
    filename = get_name_of_file_to_use()
    file_as_list = get_lines_list_of_file(filename)
    print_list_as_numbered_list(file_as_list)


def main_question_4():
    question = """

    4. Item Counter
    Assume that a file containing a series of names (as strings) 
    is named names.txt and exists on the computer’s disk. Write a 
    program that displays the number of names that are stored in 
    the file. (Hint: Open the file and read every string stored 
    in it. Use a variable to keep acount of the number of items 
    that are read from the file.)
    """
    print(question)
    # Define filename to use for this quesiton
    filename = 'names.txt'

    # get a list of lines from the file that will represent number of names.
    list_of_names = get_lines_list_of_file(filename)

    # store the number of names as a vriable and then print out that number
    number_of_names = len(list_of_names)
    print(
        f'The number of names that are stored in this file is {number_of_names}')


def main_question_5():
    question = """

    5. Sum of Numbers
    Assume that a file containing a series of integers is named 
    numbers.txt and exists on the computer’s disk. Write a program 
    that reads all of the numbers stored in the file and calculates 
    their total.
    """
    print(question)
    # Create a numbers.txt file randomly populated with integers
    create_numbers_file()

    # Define filename to use for this quesiton
    filename = 'numbers.txt'

    # get a list of lines from the file with a list of numbers.
    list_of_numbers_str = get_lines_list_of_file(filename)

    # convert list to integers
    list_of_numbers_int = convert_to_integer(list_of_numbers_str)

    # establish an accumulator and sum up every number in the list
    accumulator = 0
    for num_int in list_of_numbers_int:
        accumulator += num_int

    print(f'The sum of the number stored in this file is {accumulator}')


def convert_to_integer(str_list):
    """
    This function takes a list to make another list with each value being an integer
    if a value is not an integer it will report an error as such.
    """
    # create an empty list
    num_list = []
    try:
        for string_num in str_list:
            num_list.append(int(string_num.strip()))
        return num_list
    except:
        print(
            f'ERROR: The list provided of: \n  {str_list} \n did not have a string stored as an integer. EXITING ')


def get_name_of_file_to_use():
    """
    function that asks user for a name of a file and returns that name
    """
    try:
        return input("Please provide me with a filename to use [Enter for default(numbers.txt)]:  ") or 'numbers.txt'
    except:
        print("something didn't happen correctly, please try again: \n")
        filename = get_name_of_file_to_use()
        return filename


def get_lines_list_of_file(filename='test.txt'):
    """
    function to open a file and return a list of the lines
    if the filename is incorrect, user is given the option to
    input the correct filename
    """
    list_to_return = []
    try:
        with open(filename, 'r') as rf:
            return rf.readlines()

    except:
        print(f""" 
        There was a problem, please ensure {filename}
        is correct and try again: """)
        filename = get_name_of_file_to_use()
        list_to_return = get_lines_list_of_file(filename)
        return list_to_return


def print_list_as_numbered_list(my_list):
    """
    function to print all values in a list with sequential
    numbers starting at 1
    """
    accumulator = 1
    for item in my_list:
        print(f'{accumulator}: {item.strip()}')
        accumulator += 1


def display_first_five_lines_of_file():
    """
    Create a function that read and display the
    first 5 lines (or less if there are less than
    5 lines in the file)
    """
    filename = input("Pleaes provide me with a file to display 5 lines [Enter for default]: ") or 'numbers.txt'
    try:
        with open(filename, 'r') as readfile:
            read_in_file = readfile.readlines()
            if len(read_in_file) < 5:
                for line in read_in_file:
                    print(str(line).strip())
            else:
                for line in range(5):
                    print(str(read_in_file[line]).strip())
    except:
        print(f'The file provided [ {filename} ] was not correct, please supply another one. ')
        display_first_five_lines_of_file()


def create_numbers_file():
    """
    Creates a numbers.txt and fills it with a random number
    integers.
    """
    with open('numbers.txt', 'w') as outfile:
        # Write serios of integers.
        for number in range(1, random.randint(2, 50)):
            outfile.write(str(number * random.randint(-21, 21)) + '\n')


def display_numbers():
    """
    displays all the lines in numbers.txt of the root folder
    """
    with open('numbers.txt', 'r') as display_infile:
        print(display_infile.read())




# call main function
main_question_1()
main_question_2()
main_question_3()
main_question_4()
main_question_5()
