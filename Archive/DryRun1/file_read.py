# this program reads and displays the contents
# of the philosophers.txt file

def main():
    # Open a file names philosophers.txt
    infile = open('philosophers.txt')

    # Read the files contents
    file_contents = infile.read()

    # Close the file
    infile.close()

    # Print the data from the file
    print(file_contents)

# Call the main function
main()
