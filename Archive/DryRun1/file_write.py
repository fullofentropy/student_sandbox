"""
When a program needs to save data for later user, it writes the data in a file.
The data can be read from the file at a later time.

'r'     - open a file for reading
'w'     - opens a file for writing
'a'     - opens a file to be written to, appended to the end

"""

# This program writes three lines of data to a file

def main():
    # Open a file named philosophers.txt

    outfile = open(r'philosophers.txt', 'w')

    # Write the names of the three philosophers.
    outfile.write('John Locke\n')
    outfile.write('Emmanuel Kant\n')
    outfile.write('David Hume\n')

    # close the file.
    outfile.close()

# Call the main function
main()
