**PART II TEACHING PLAN: Regular Expressions**

**LESSON, ATTENTION, MOTIVATION**:  Morning, everyone!  For this next section of material, we'll be covering a topic that is supremely useful, but can be difficult to master.  A technique that is often used within programming and software development is pattern recognition or matching.  Perhaps you've been given a short text and asked to count the number of times the word "the" appears in the text.  
Has anyone seen a word cloud? 
[PICTURE OF A WORD CLOUD MIGHT BE USEFUL HERE]  
If you been given a long legal document, and you're looking for a specific quote that a defendant made, but you're not sure when they did,  
(Q) how would you find that phrase in the document?  
The way that many developers use pattern matching for text nowadays is through the use of regular expressions.  Simply put, we compare a token (substring) consisting of conditions that we want to find against a text that we're looking through and then returning the results.  Sounds simple on its face, but becomes complicated very quickly when we have multiple conditions and some might conflict, if not in the correct order.  Additionally, regular expressions can be very useful to validate user input data, as users have been known to put whatever they want in text boxes.

**NAVIGATE TO OVERVIEW**

**NAVIGATE TO OBJECTIVES**

Again, if you have any questions, feel free to either ask on voice chat or post the question in the classroom chat.

Before we begin working with regular expressions, I want everyone to navigate to this url [https://regexr.com/].  There are many different regular expression builders out there, this just happens to be one that I use frequently.  In the top section, under expression, is where we'll be making our token (which I'll define here in a moment) and under text section, we can put different string text to test our token against.  For our purposes of learning, I've provided some sample text in the chat that I'll be using in the instruction.  Any text that is highlighted is text that our token will find.

**[SAMPLE TEXT]**

```
or a portion of them at least, were taken prisoners, and just as the
poor fellows in the garden were exulting in the thought that in a few
moments more freedom would be within their grasp, they found themselves
surrounded by Turkish troops, horse and foot. The Dorador had revealed
the whole scheme to the Dey Hassan.  In a house, with a hose, ha se

abbbbbbbbbbbbbbbbbbbbbbbbbbb

Lies before me, base clown!
```

Let's go over some of the basics with regular expressions, so that you can get a handle on the concept.  Using regular expressions has two components, the token that is used to search for a pattern, and the original text that is being searched against.  Within that token, there are the conditions that we will search for.  To start, we could first search for the word 'horse' with the story of Don Quxiote.  Any word within that text that matches the string 'horse' will be returned.  Interesting, but only a basic start.  A deeper search might be to find any sentence that includes the word 'horse'.

(Q) Now, how might we accomplish that? [AFTER SOME GUESSES]

We'll need to modify that token with something called metacharacters.

Metacharacters are similar to the special characters that we use for strings in that they modify how the string behaves, rather in this case the metacharacters will modify our token.  

Our first meta is the ., it matches exactly one occurence, doesn't matter which (and this includes whitespace, but not newline characters).  So, if our token is 'h..se', 
(Q) what group of characters would be good matches?   
**[HORSE, HOUSE, MAKE SURE TO MENTION ONE WITH WHITESPACE 'HA SE' FOR EXAMPLE]**

The next one is *. This searches for zero or one occurrences to come before a character, so importantly, the * meta cannot be used by itself.  It must modify the meaning of another regex (before it).  

(Q) What will the token fel* find in our text?  
(A) fell from fellows and fe from few

Third up, is the ?.  This matches zero or one characters and again, this meta modifies the regex in front of it.  horses? will find either Horse or Horses.  Additionally, using *? with another regex will use a nongreedy or lazy pattern matching.  This means that once the pattern has been found, the result will be returned, instead of trying to find the largest result possible.  

(Q) How will the token fel* results be modified by changing to 'fel*?'?  
(A) The results are limited to the two fe

Similar to *, is the +, which looks for at least one occurrences.  ab+ will find both ab and abbbbbbbbbbbbbbbbbbbbbbbbbbb.  Just like *, + is a greedy matching regex, so if we want to limit to the smallest match, we need to combine it with +?.

These four metas will typically be your first choice when needing to modify your token, but there are others that might be useful.

The {} indicate a number of occurrences of the previous regex (again this modifies the regex before it).  This can be used either matching an exact number of occurrences, {#}, or a range of occurrences, {x, y}, meaning match between x and y occurrences of the regex.  If {#,} is used, then a match is found when at least that number of occurrences is found.

(Q) If we want to find at least 2, but no more than 5 occurrences of a character, how would we use {}?  
(A) {2, 5}

The () is used to isolate the match from the other conditions.  Similar to how () might be used for logic statements, in our token, matches within a () will be done first before trying to match the other conditions.

The [] indicate a range of characters that will be used to match.  Typically used with digits or letters, if you type [0-9], your token will match a single digit.  [a-z] will match any lowercase letters, [A-Z] any uppercase characters.  We can also narrow this down like this [G-T].  

(Q) What will this range match?  
(A) ANY CHARACTER BETWEEN G AND T.

The allowed characters can also be customized, such as vowels [aeiou], appropiate number endings [thnds] (st, nd, th), a combination of numbers and letters [345aing], etc.
To negate a custom class, we use the ^ character, so [^a-z] looks for all non-lowercase characters.  Use of a metacharacter in a custom class is also treated as the character as a literal, so [*abc] will look for the * in addition to looking for a, b,and c.  This can also be done outside of a custom class through the use of the metacharacter \.

(Q) How would be look for all non-digit characters using custom classes?
(A) [^0-9]

Equivalent to [0-9] is the predefined character class \d.  If we want to find the opposite of this, i.e. not a digit, then we use the opposite case, so \D. \w is the alphanumeric character class (letters, number, and underscore), \W is any non-alphanumeric character. \s is any whitespace character (spaces, tabs and newlines) and \S is any non-whitespace character.

An interesting set of metacharacters are the ^ and \$.  Outside of the custom class, ^ indicates where the beginning of a match starts and \$ indicates where the end of a match is.  For the string 'Lies before me, base clown!', ^Lies would match the beginning of the string and clown!\$ would match the end of the string.  If, however, we tried ^clown, we would not get a result, since clown is not the beginning of the string (and the same would of before$, as that is not the end of the string).

This has to be done to each string individually.  For our large text, even though there's multiple sentences present, it is count as one string on the page.  So ^ will only match starting with or, and \$ will only match ending with clown!.  If we wanted to match with multiple sets of string, and test ^ and \$ against all of them, we can use the Tests tab.

Lastly, we can use the | character.  Similar to its use as a logical operator, | will evaluate to see if either pattern provided matches (and typically this is best done with the use of ()).

We've gone over a lot of the basics of regular expressions in general, so let's review some of concepts.

(Q) How do we use {} to control the range of occurrences found?  
(A) {n, m}

(Q) What condition will * match?  
(A) If the previous regex has zero or more occurrences.

(Q) What does the | operator do?  
(A) Will evaluate to see if either provided pattern matches.

(Q) When combined with either * or +, what does the ? meta character do?  
(A) It will use a non-greedy match, and limit the result to the smallest match

(Q) What metacharacter do we use for a custom character class?  
(A) []

**ONE LESSON LENGTH**

I am going to go over some examples to show these metacharacters in action.

A classic example used for regular expressions is finding a phone number.  Used in a variety of scenarios, you've probably had to input your phone number (or a phone number) into a field online.  
(Q) Can anyone give me an example where they've done this?   
**[GO OFF OF BUSINESS WEBSITE ANSWER]**

That's right, most e-commerce website ask for a phone number, at its face to help the customer if something goes wrong with the order.  So, here in the states,  
(Q) what is the format of a phone number?  
**[xxx-xxx-xxxx, STUDENTS MAY SKIP AREA CODE, OR MAY BRING UP PARENTHESES]**  
Let's start with the digits first.  We're going to have three groups of digits, so let's represent that with d d d.  
**[WILL BE HELPFUL TO EITHER PRESENT THIS IN A TEXT DOCUMENT, https://regexr.com/, OR PAPER.]**  
The first group of digits will have three numbers between 0-9, so that can represented either \d\d\d or \d{3} or [0-9]{3}, or [0-9][0-9][0-9].  The second group of digits will again have three numbers  
(Q) so what should our token look for that?  
**[ANSWERS WERE PROVIDED FOR FIRST GROUP]**  
Lastly, the end group will have 4 numbers,  
(Q) so what will be our token for this group?  
**[AGAIN REFER TO FIRST GROUP, BUT USE 4 INSTEAD OF 3.]**  
So our token so far looks like \d{3}\d{3}\d{4}.  However, this will only work if phone number has no spaces, dashes, or parentheses, as many representations of a phone number do, xxxxxxxxxx.

Let's modify our token to account for these edge cases.  First, spaces and dashes between the numbers. If we add a - between the numbers or a space, so xxx-xxx-xxxx or xxx xxx xxxx, we'll be able find results individually.
(Q) How do we combine them so that the token will find either of them?   
**[USE OF THE |]**  
Just like if we to solve a logical statement, using the or operator | will allow us to find either of them, so our token will now look like \d{3}(-| )\d{3}(-| )\d{4}, where the first group is three digits, followed by either a - or space, then 3 more digits, another - or space, and finally 4 digits.  As we're building this, you made be thinking to yourself, "This looks an awfully like a social security number."  And you would be correct.  Social security numbers and phone numbers share many of same formatting rules, so you'll need additional logic to handle that distinction.
One of the last things that can be in a phone number are parentheses, and these can be optional.  Since a parathese is a special character, we'll need to escape it, and since we need either zero or one of the character, we'll use a ?.  So the beginning of the token will look like \(?.  That covers the left parathese and now for the right.  
(Q) How would we build the right side?  
**[SPECIAL CHARACTER ESCAPE \, THE RIGHT PARATHESE ), ZERO OR ONE ?]**  
Combining these results with our previous build yields \(?\d{3}\)?(-| )\d{3}(-| )\d{4}.

Having built this token, we can use this find a phone number within a text.  Additionally, we could use this to validate a string returned from a user input field, to see if what the user input is a valid string, and if not, to inform them of the error.  Now, the choices I made for this token do have some flexibility.  We've already gone over the face that searching for the digits can be represented by either \d{3} or [0-9]{3}.  It can be argued that the second example is a bit more readable and immediately understandable, while the first example is more efficient.  For the section dealing with the dashes, instead of searching for a zero or one occurrence of ( and ), we could have enclosed the first section in its own parentheses and searched for either \d{3} or \(\d{3}\), so (\(\d{3}\)|\d{3})(-| )\d{3}(-| )\d{4}.

Now, the first token does have a flaw in it.  
(Q) Can anyone tell me what the first token might additionally pick up?   
**[MIGHT FIND A RESULT THAT HAS THE '(' BUT NOT THE ')' FOR THE FIRST SET OF DIGITS.]**  
For regular expressions, it can be difficult to make sure that a specific result is not returned, if the search token is too general.

Let's review some of the topics presented in this demonstration.

(Q) What does the (-| ) condition look for?  
(A) Either a - or a space.

(Q) What does the \d{4} condition look for?  
(A) Four digits

(Q) How do we escape meta characters?  
(A) With the use of the \ character.

For the previous sections, we've talked about regular expressions in a general sense, how it works in a language agnostic way.  Now we're going to go over some of specific ways that Python handles regular expressions.  Python has the re library in the Standard Library and re uses several specific functions to analyze a text.  
A good default function to use is re.compile() which generates a regular expression object, based on the pattern provided.  This allows for a pattern to be reused multiple times to search, instead of a string needing to be referenced again and again.

The actual functions that are used to search a text are re.match, re.search, and re.findall, and all behave slightly differently.  
Re.match() is used to find a pattern that matches at the beginning of the string.  
Re.search() searches the whole string, to see if a match is found.  
Re.findall() will find all occurrences of a pattern and return the result as a list.  These functions will return None if a result is not found (according to the rules of that function).  

A function that can be useful for checking our pattern tokens is re.fullmatch(), which will return a result if the pattern matches the whole string, not just contained within.  

A function that can be used for find and replace is the re.sub() function, which will take a pattern, what will replace that pattern once found, and the text to search through.  This will also return the resulting string.  The function can also use a count argument, to limit the number of times that the pattern is replaced.  
The re library also has a form of the split function, where a token is used as a delimiter for a string, and that string is tokenized based on the delimiter.  The result will be a list of strings.  
Two final notes about the re library. Instead of getting all matches all at once, we can use the re.finditer() to get an iterable of the match objects.  This can be useful if we're using a for loop to go over each result or perhaps using list comprehension to transform the results in some fashion.  Lastly, each of the search function can be modified with re.IGNORECASE flag, so that the search can be done case-insensitive.

We've gone over some of the functions available in the re library, let's review what they do.

(Q) What is the difference betweeen re.search() and re.match()?  
(A) .search() will go through the whole document.  .match() will only match at the beginning of the string

(Q) What function can be useful for checking if a search pattern will fully match a string?  
(A) re.fullmatch()

(Q) What flag can be used with the re search functions to do a case-insensitive search?  
(A) re.IGNORECASE

(Q) What does the re.findall() return?  
(A) A list of all found matches

**LESSON REVIEW**