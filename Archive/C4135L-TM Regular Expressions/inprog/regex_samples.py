import re

mystring = """uTesday,0 0February 8, 2|22 4:05:05 AM  d2345_ 2 F3222 Tuesday, February 7, 2021 4:12:05 AM
# Friday, 0Ferbuary 1, 2013 9:05:05 AM  Friday, February 1, 1999 2100 12999 199 12:05:05 PM"""

# determin the desired pattern

print(mystring)
regex_search_string = r'\d{4}'
match_list = re.findall(regex_search_string, mystring)
print('--Patternprint:',regex_search_string)
print(match_list)
print('----')
regex_search_string = r'\d\d\d\d'
match_list = re.findall(regex_search_string, mystring)
print('--Patternprint:',regex_search_string)
print(match_list)
print('----')
regex_search_string = r'\s\d{4}\s'
match_list = re.findall(regex_search_string, mystring)
print('--Patternprint:',regex_search_string)
print(match_list)
print('----')
regex_search_string = r'\s(\d{4})\s'
match_list = re.findall(regex_search_string, mystring)
print('--Patternprint:',regex_search_string)
print(match_list)
print('----')
regex_search_string = r'0[0-9Febr]*'
match_list = re.findall(regex_search_string, mystring)
print('--Patternprint:',regex_search_string)
print(match_list)
print('----')
regex_search_string = r'\D(\d\d\d\d)(\W)'
match_list = re.findall(regex_search_string, mystring)
print('--Patternprint:',regex_search_string)
print(match_list)
print('----')
regex_search_string = r'[^4-9][0|9]\d{2,}|[0-9][|0-9][0-9]{2}'
match_list = re.findall(regex_search_string, mystring)
print('--Patternprint:',regex_search_string)
print(match_list)
print('----')



# regex_search_string1 = r'\D(\d\d\d\d)(\W)'
# regex_search_string1 = r'[^4-9][0|9]\d{3,}|[0-9][0-9][0-9]{2}'
# mypattern = re.compile(regex_search_string1)

# matches = mypattern.finditer(mystring)


# # pythonic list comprehention matching
# [ print('Object:',_,"\nValue:", _.group(0)) for _ in matches ]

# # reset the iterator so we can apply the for loop
# matches = mypattern.finditer(mystring)

# # traditional for loop to do.
# for _ in matches:
#     print(f"""
#     Object: {_}
#     Value: {_.group(0)}
#     Start: {_.start()}
#     Stop: {_.end()}""")
    