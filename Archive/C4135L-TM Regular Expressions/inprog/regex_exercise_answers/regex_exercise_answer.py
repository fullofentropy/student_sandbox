### Answers created by Richard Boettcher richard.boettcher@us.af.mil or fullofentropy@gmail.com

import re

def q1():
    string1 = '1. Recognize the following strings: “bat,” “bit,” “but,” “hat,”, “hit,” or “hut.”'
    print('Question 1\n',string1)
    pattern1 = re.compile = r'[bh]{1}[iau]{1}t'
    result1 = re.findall(pattern1, string1)
    print(result1)

def q2():
    question2 = '2. Match any pair of words separated by a single space, that is, first and last names.'
    string2 = 'Firstname Lastname John Smith Brian Lee Edward Allen'
    print('\nQuestion 2\n',question2)
    # simple answer
    pattern2 = r'[A-Za-z\'-]+ [A-Za-z\'-]+'
    # a little more complex
    pattern3 = r'[A-Za-z\'-]+ [A-Za-z\'-]+'
    result2 = re.findall(pattern2, string2)
    print(result2)

def q3():
    string3 = """3. Match any word and single letter separated by a comma and single space, 
as in lastname, firstinitial.  Johns, is happy, F Sleepy, B kdjeis
    """
    print('\nQuestion 3\n',string3)
    pattern3 = r'\w+, \w'
    result3 = re.findall(pattern3, string3)
    print(result3)

def q4():
    from keyword import kwlist
    string4 = '4. Match the set of all valid Python identifiers. perhaps try 1Test, try.thisdot try.1dot Python , print python.'
    print('\nQuestion 4\n',string4)
    pattern4 = r'([a-zA-Z_][a-zA-Z0-9]+)'
    result4 = re.findall(pattern4, string4)
    print("Valid python identifiers:\n\t",set(result4))
    print("Valid python identifiers minus common keywords:\n\t", set(result4).difference(kwlist))

def q5():
    print('\nQuestion 5')
    string5 = """ 
    5. Match a street address according to your local format (keep
    your regex general enough to match any number of street
    words, including the type designation). For example, American
    street addresses use the format: 1180 Bordeaux Drive. Make
    your regex flexible enough to support multi-word street
    names such as: 3120 De la Cruz Boulevard. 
    Another valid US address is S93W5456 Catfish ln. of craziness 
    """
    print(string5)
    # pattern5_too_simple = r'([0-9]+) ([\w ]+)'
    pattern5 = r'([0-9]?[nsewNSEW]?[0-9]+?[nsewNSEW]?[0-9]+) ([\w ]+)'
    result5 = re.findall(pattern5, string5)
    print(result5)    

def q6():
    print('\nQuestion 6')
    string6 = """ 
6. Match simple Web domain names that begin with “www.”
and end with a “.com” suffix; for example, www.-yahoo.com.
Extra Credit: If your regex also supports other high-level
domain names, such as .edu, .net, etc. (for example,
www.foothill-.edu).
"""
    print(string6)
    string6 += """webmagnat.ro www.nickelfreesolutions.com www.scheepvaarttelefoongids.nl www.tursan.net www.plannersanonymous.com www.doing.fr www.saltstack.com www.deconsquad.com www.migom.com www.tjprc.org www.worklife.dk www.inno-make.com www.food-hub.org www.bikemastertool.com www.betenbewegen.de www.vk.me www.twotigersports.com www.517mrt.com www.siel.nl www.e-hps.com www.infowheel.com www.synirc.net www.abuliyan.com www.easy-ways.com www.stark.dk www.funwirks.com www.eurocqs.net www.202yx.com www.nikko-sake.com www.xnet.lv www.visionpharma.com www.trade-india.com www.t-bird.edu www.siebel-crm.net www.adriaticapress.it www.41789.com www.autofirenze.biz www.minniowa.com www.sweetteaproper.com www.recruit-dc.co.jp www.competitivecameras.com www.zoncinta.com www.vadim-prostakov.ru www.vk.me www.securitasweb.it www.mandarinclass.my www.pbxi.net www.zyznowski.pl www.meodia.com www.niceinternetmoney.ru www.guardiancaps.com www.bbccable.net www.ams-luenen.de www.ihalehaberbulteni.com www.salvia-community.net www.superpowerfruits.com www.bereyellingergl.com www.downloadfreetvseries.com www.iqshop.ro www.urbancliq.com www.rocket-media.net www.uskudarhaliyikama.org www.ftbp.ro www.savedeo.com www.nanabit.net www.memewood.com www.divorcereform.us www.solarincomefund.com www.novofri.com www.8852088.com www.admil.ru www.rafonline.org www.bois-francs.com www.pieceinch.com www.tel.com www.teenpregnancysc.org www.wisdomforhealth.net www.powgnar.com www.screenmediamag.com www.pricerabbit.com www.archive-host.fr www.hotimpit.ru www.detskie-krovatki.by www.3sitracking.com www.hellfiremods.co.uk www.eslyoga.com www.buyphen375cheap.com www.kilgour.com www.vk.me www.hualienhouse.com www.tour-box.ru www.servizioallerta.it www.denyojp.com www.2912muranoway.info www.centralepiecesauto.com www.bluedynamics.com www.xjs123.com www.veryicon.com www.emrekoldas.com www.adc-inspect.com www.vusadbe.by www.morban.co.uk www.hainanair.us www.vk.me www.metar.gr www.reviewsearchfree.com www.treasureislandbar.com www.mobilier-bebe.com www.bvillaseminyak.com www.sponsoredhomepage.com www.santifortwo.com www.pcrecruiter.net www.bcdtofu.com www.thefavorite.fr www.aneventapart.com www.pces.in www.alamelphan.com www.hotel63.com www.kitubi.com www.zenrog.com www.kinderroller.org www.naverisk.com www.teme.us www.tlinkbroadband.com www.de.vc www.rivnetwork.com www.bufobufo.eu www.friatider.se www.mannllc.com www.rideukbmx.com www.agnx.com www.stackablehost.com www.muzed.co.uk www.up247.me www.vk.com www.urt.is www.notabilus.com www.jordansun.com www.frosta.hu
"""
    # Note: (?!-) is a negative look ahead,  (?<!-) is a negative look behind 
    # Lookaround    Name    What it Does
    # (?=foo)    Lookahead    Asserts that what immediately follows the current position in the string is foo
    # (?<=foo)    Lookbehind    Asserts that what immediately precedes the current position in the string is foo
    # (?!foo)    Negative Lookahead    Asserts that what immediately follows the current position in the string is not foo
    # (?<!foo)    Negative Lookbehind    Asserts that what immediately precedes the current position in the string is not foo
    # example:
    # pattern6 = r'www\.(?!-)[A-Za-z0-9\-]{1,63}(?<!-)\.(?:com|edu|net|mil|org|us)'
    #   (?!-)  This will look ahead after the . to see if "-" exists and not include it if it is there for the match
    #   (?<!-)  This will look behind as soon as the "." is matched to see if "-" exists and not include it if it is there for the match
    #   This basically demonstrates a way to remove a "-" even if it is included in the [] as something to match with
    pattern6 = r'www\.[A-Za-z0-9\-]{1,63}\.(?:com|edu|net|mil|org|us)'
    result6 = re.findall(pattern6, string6)
    print(result6)

def q7():
    print('\nQuestion 7')
    string7 = """ 
    7. Match the set of all valid e-mail addresses (start with a loose
    regex, and then try to tighten it as much as you can, yet
    maintain correct functionality).  Example: stevebalrog@minesofmoria.com,
    tom.bombadil@us.af.mil, lord_of_the_strings@the-shire.co.nz
    valid emails:

    mkyong@yahoo.com, mkyong-100@yahoo.com, mkyong.100@yahoo.com, mkyong111@mkyong.com, 
    Catz-100@meow.net, catfish.100@mkyong.com.au, cherry-egz@1.com, facman_98@gmail.com.com, 
    youknowme+100@gmail.com, iknowyou-100@yahoo-test.com
    stevebalrog@minesofmoria.com\n,
    tom.bombadil@us.af.mil, lord_of_the_strings@the-shire.co.nz

    invalid emails:

    mkyong, mkyong@.com.my, mkyong123@gmail.a, 
    mkyong123@.com, mkyong123@.com.com, .mkyong@mkyong.com, mkyong()*@gmail.com, 
    mkyong@%*.com, mkyong..2002@gmail.com, mkyong.@gmail.com, mkyong@mkyong@gmail.com, 
    mkyong@gmail.com.1a
    """
    print(string7)
    # Note: 
    # (?![look fwd do not match if exists]) is a negative look ahead,  
    # (?<![look behind, do not match if exists]) is a negative look behind  
    # (?:[match something]|[or another thing])
    pattern7 = r'(?:\s|^|,| )(?!\.)(?:[a-zA-Z0-9._+-](?!\.\.))+(?<!\.)@(?!\.)[a-z0-9.-]+\.(?:com|net|au|mil|nz)(?!\.)'
    result7 = re.findall(pattern7, string7)
    # strip_newlines = [_.strip() for _ in result7]
    # print(strip_newlines)
    [print(_) for _ in result7]

def q8():
    print('\nQuestion 8')
    string8 = """
    8. Match the set of all valid Web site addresses (URLs) (start
    with a loose regex, and then try to tighten it as much as you
    can, yet maintain correct functionality).  Example: can be found
    by using sample8.txt
"""
    samplefile = 'sample8.txt'
    print(string8)
    # add all text from samplefile as a string to string8 variable
    with open(samplefile) as sampledata:
        string8 += sampledata.read()
    # alt pattern = r'\w+://[\w\-\.]+/\w+.\w+'
    pattern8a = r'\w+://?[\w\.]+[A-Za-z0-9\-]{1,63}\.(?:com|gov|edu|net|mil|org|us)/(?:\w+.)+\.?\w+'
    # influenced from https://urlregex.com/
    pattern8b = r'\w+://?(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    # # pattern8b = r'(\w+)://([\w\-\.]+)/(\w+).(\w+)'
    result8a = re.findall(pattern8a, string8)
    result8b = re.findall(pattern8b, string8)
    # print(strip_newlines)
    print("----results----")
    print('Pattern a count',len(result8a))
    print("Pattern b count",len(result8b))
    with open('q8a.txt', 'w') as wf:
        for value in result8a:
            wf.writelines(value+'\n')
    with open('q8b.txt', 'w') as wf:
        for value in result8b:
            wf.write(value+'\n')

def q9():
    print('\nQuestion 9')
    string9 = """
Playing with the type() built-in function that returns an object of type, type with description of the type of object. 
Write a function that:
    (1) uses regex to find the name of the type(some_object) that is returned and print this value to console
        Example:
            >>>returned_type_object = type(100)
            >>>returned_type_object 
            <class 'int'>
        In the example we would use regex to find "int" and nothing else.
            >>>returned_type_object = type(100.01)
            >>>returned_type_object 
            <class 'float'>
        In the example we would use regex to find "float" and nothing else.

    (2) determine if the Base type of the type is of type "type"return True, if it isn't, return the string of its base type, None for anything else.
        Example:  True should be returned
            >>>type(int)
            <class 'type'>

        Example: 'builtin_function_or_method'should be returned
            >>>type(oct)
            <class 'builtin_function_or_method'>
            >>> type(builtin_function_or_method)
            Traceback (most recent call last):
            File "<stdin>", line 1, in <module>
            NameError: name 'builtin_function_or_method' is not defined

        Example: 'NoneType' should be returned.
            >>>type(None)
            <class 'NoneType'>
            type(NoneType)
            Traceback (most recent call last):
            File "<stdin>", line 1, in <module>
            NameError: name 'NoneType' is not defined

Note: the python builtin dictionary containing builtin objects is: __builtins__.__dict__
    >>>type(__builtins__.__dict__)
    <class 'dict'>
This will be a dictionary in which the "key" is the name and the "value" is the object
    >>>__builtins__.__dict__['bool'] 
    <class 'bool'>
    >>>type(bool)
    <class 'type">

Note: Running in debug mode will give you false errors associated with accessing __builtins__.__dict__


A pythonic way to get all type objects
>>> builtin_types=  [ _ for _ in __builtins__.__dict__.values() if isinstance(_, type)]
"""
    print(string9)
    def somefunction(obj, count=0, print_more=None):
        """
        What needs to be provided: 
        obj: this is what is passed in for answer the question.
        
        Nice to have additions used for debuging:
            count: count recursive calls 
            print_more: used to provide more console output to better understand ctrl flow

        Note: Running in debug mode will give you false errors associated with accessing __builtins__.__dict__
        """
        if print_more:
            # This lets us know what recursive iteration
            # we are in for understanding control flow
            print("count:",count) 

        # Add a try/except to see if the provided data can be pointed to an object.
        try:
            tempstring = str(type(obj))
            if print_more:
                print('String to Regex:',tempstring)
            regexpattern =  r"(?<=')\w+(?=')"
            type_as_string = re.findall(regexpattern, tempstring)[0]

            # printing out first requirement
            print(f"Found Object -->{str(obj)}<-- \nis of type: -->{type_as_string}<-- as found using Regex")

        except NameError as theError:
            if str(theError).find('re'):
                print("you forgot to import the regular expression builtin module....")
            if print_more:
                print("count:",count,"NameError:", theError)
            print('Returning None')
            return None
        # if it turns out the type provided is of type type, we return True
        if type_as_string == 'type':
            if print_more:
                print("The type of object is of type, type and thus returning true: ",type_as_string)
            return True
        else:
            # Since it isn't of type "type" we will check to see if this will be of type type.
            try:
                # This tries to see if the object exits in the builtins dict, 
                # if not, it does not have a key in the dictionary and will 
                # produce a key error.  
                # If this happens we simply return the string value
                # found by RegEx
                try:
                    result = __builtins__.__dict__[type_as_string]
                except KeyError as thekeyerror:
                    if print_more:
                        print("KeyError",thekeyerror)
                        print(type_as_string)
                    print("This is not of type, type")
                    return type_as_string
                if print_more:
                    print("count:",count,'Not yet type of type object:',result)
                result_recursive = somefunction(__builtins__.__dict__[type_as_string], count+1)
                if print_more:
                    print("count:",count,"result_recursive:",result_recursive)
                return result_recursive
            except NameError as theError:
                if print_more:
                    print("NameError:", theError)
                    print('Returning:',type_as_string)
                    print(count)
                return type_as_string    
    testData = [0,1.2,'some string',oct,None,print]
    print("Sending in Test data:",testData)
    for data in testData:
        print("----")
        print(somefunction(data))




def q10():
    pass

def q11():
    pass

# q1()
# q2()
# q3()
# q4()
# q5()
# q6()
# q7()
# q8()
q9()
# q10()

