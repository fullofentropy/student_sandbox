import re

def q9():
    print('\nQuestion 9')
    string9 = """
Playing with the type() built-in function that returns an object of type, type with description of the type of object. 
Write a function that:
    (1) uses regex to find the name of the type(some_object) that is returned and print this value to console
        Example:
            >>>returned_type_object = type(100)
            >>>returned_type_object 
            <class 'int'>
        In the example we would use regex to find "int" and nothing else.
            >>>returned_type_object = type(100.01)
            >>>returned_type_object 
            <class 'float'>
        In the example we would use regex to find "float" and nothing else.

    (2) determine if the Base type of the type is of type "type"return True, if it isn't, return the string of its base type, None for anything else.
        Example:  True should be returned
            >>>type(int)
            <class 'type'>

        Example: 'builtin_function_or_method'should be returned
            >>>type(oct)
            <class 'builtin_function_or_method'>
            >>> type(builtin_function_or_method)
            Traceback (most recent call last):
            File "<stdin>", line 1, in <module>
            NameError: name 'builtin_function_or_method' is not defined

        Example: 'NoneType' should be returned.
            >>>type(None)
            <class 'NoneType'>
            type(NoneType)
            Traceback (most recent call last):
            File "<stdin>", line 1, in <module>
            NameError: name 'NoneType' is not defined

Note: the python builtin dictionary containing builtin objects is: __builtins__.__dict__
    >>>type(__builtins__.__dict__)
    <class 'dict'>
This will be a dictionary in which the "key" is the name and the "value" is the object
    >>>__builtins__.__dict__['bool'] 
    <class 'bool'>
    >>>type(bool)
    <class 'type">

Note: Running in debug mode will give you false errors associated with accessing __builtins__.__dict__


A pythonic way to get all type objects
>>> builtin_types=  [ _ for _ in __builtins__.__dict__.values() if isinstance(_, type)]
"""
    print(string9)
    def somefunction(obj, count=0, print_more=None):
        """
        What needs to be provided: 
        obj: this is what is passed in for answer the question.
        
        Nice to have additions used for debuging:
            count: count recursive calls 
            print_more: used to provide more console output to better understand ctrl flow

        Note: Running in debug mode will give you false errors associated with accessing __builtins__.__dict__
        """
        if print_more:
            # This lets us know what recursive iteration
            # we are in for understanding control flow
            print("count:",count) 

        # Add a try/except to see if the provided data can be pointed to an object.
        try:
            tempstring = str(type(obj))
            if print_more:
                print('String to Regex:',tempstring)
            regexpattern =  r"(?<=')\w+(?=')"
            type_as_string = re.findall(regexpattern, tempstring)[0]

            # printing out first requirement
            print(f"Found Object -->{str(obj)}<-- \nis of type: -->{type_as_string}<-- as found using Regex")

        except NameError as theError:
            if str(theError).find('re'):
                print("you forgot to import the regular expression builtin module....")
            if print_more:
                print("count:",count,"NameError:", theError)
            print('Returning None')
            return None
        # if it turns out the type provided is of type type, we return True
        if type_as_string == 'type':
            if print_more:
                print("The type of object is of type, type and thus returning true: ",type_as_string)
            return True
        else:
            # Since it isn't of type "type" we will check to see if this will be of type type.
            try:
                # This tries to see if the object exits in the builtins dict, 
                # if not, it does not have a key in the dictionary and will 
                # produce a key error.  
                # If this happens we simply return the string value
                # found by RegEx
                try:
                    result = __builtins__.__dict__[type_as_string]
                except KeyError as thekeyerror:
                    if print_more:
                        print("KeyError",thekeyerror)
                        print(type_as_string)
                    print("This is not of type, type")
                    return type_as_string
                if print_more:
                    print("count:",count,'Not yet type of type object:',result)
                result_recursive = somefunction(__builtins__.__dict__[type_as_string], count+1)
                if print_more:
                    print("count:",count,"result_recursive:",result_recursive)
                return result_recursive
            except NameError as theError:
                if print_more:
                    print("NameError:", theError)
                    print('Returning:',type_as_string)
                    print(count)
                return type_as_string    
    testData = [0,1.2,'some string',oct,None,print]
    print("Sending in Test data:",testData)
    for data in testData:
        print("----")
        print(somefunction(data))


q9()