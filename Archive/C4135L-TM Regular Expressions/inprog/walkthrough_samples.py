import re

# # some  string value
# mystring = 'a b c d efgh'

# # determin the desired pattern
# mypattern = re.compile(r'\w+')

# # use some way to match/find and extract what you want
# matches = mypattern.finditer(mystring)

# # print and/or store the values

# # pythonic
# [print('Object:',_,"\nValue:", _.group(0)) for _ in matches ]

# ################################################################
# print("-----------------")
# # some  string value
# mystring = """Tuesday, February 8, 2022 4:05:05 AM    Tuesday, February 7, 2022 4:12:05 AM
# Friday, February 1, 2022 9:05:05 AM  Friday, February 1, 2022 12:05:05 PM"""

# # determin the desired pattern
# regex_search_string1 = r'\d?\d:\d\d:\d\d\s[A|P]M'
# regex_search_string2 = r'[0-2]?\d:[0-5]\d:[0-6]\d\s[A|P]M'

# mypattern = re.compile(regex_search_string1)
# mypattern = re.compile(regex_search_string2)

# # use some way to match/find and extract what you want
# matches = mypattern.finditer(mystring)

# # print and/or store the values

# # pythonic
# # [print('Object:',_,"\nValue:", _.group(0)) for _ in matches ]

# for _ in matches:
#     print(f"""
#     Object: {_}
#     Value: {_.group(0)}
#     Start: {_.start()}
#     Stop: {_.end()}""")

test = b'\x1babcallt\nhethings\x07123'
test2 = re.sub(b"\x1b.*\n?.*\x07", b'', test)
print(test2)

import re

'''
the_string = "Can't stop, won't stop"
results = re.search(r'stop', the_string)
print(results.group())
print(results)
'''

'''
# compile our re
the_string = "Can't stop, won't stop"
the_string2 = "stop the music"
the_string3 = "Never ending story"

# re.search searches the entire string
# re.match searches at the start of the string and stops if no match is immediately found

pattern = re.compile(r'stop')
results = pattern.search(the_string)
print(results)
'''

'''
the_string = "Can't stop, won't stop"
pattern = re.compile(r'stop')

# findall returns a list() of the found strings
results = pattern.findall(the_string)

# finditer returns a iterable object of re objects
results = pattern.finditer(the_string)

for result in results:
    print(result.string)
'''


r'''
anchors
^ beginning of string
$ end of string

quantifier
* zero or more
+ one or more
? zero or one
{2} exactly 2
{2,} exeactly 2 or more
{2,5} 2 up to 5

character classes
. match anything
 \w == [a-zA-Z0-9_]
'''

r'''
valid_email = "s_jobs2@apple.com"
pattern = re.compile(r'[a-zA-Z0-9_]+@\w+\.[com|net|org]+')
#pattern = re.compile(r'^\w+@[a-z]')
results = pattern.findall(valid_email)
print(results)
'''

r'''
valid_email = "My email is hunter2@live.net.  Contact me anytime!"

 # \w == [a-zA-Z0-9_]
pattern = re.compile(r'\w+@\w+\.\w+')
results = pattern.findall(valid_email)
print(results)
'''

r'''
# does cat, hat or bat exist in this string?
cool_string = "The cat in the hat."
pattern = re.compile(r'cat|hat|bat')
results = pattern.findall(cool_string)
print(results)

# does cat, hat or bat exist in this string?
cool_string = "The cat in the hat on the mat."
#pattern = re.compile(r'[a-z]at', re.IGNORECASE)
pattern = re.compile(r'([b|h|m]at)\.$')
results = pattern.findall(cool_string)
print(results)
'''

r'''
# does cat, hat or bat exist in this string?
cool_string = "The cat in the hat on the mat."
#pattern = re.compile(r'[a-z]at', re.IGNORECASE)
pattern = re.compile(r'([b|h|m]at)')
results = pattern.finditer(cool_string)

for result in results:
    print(result.start())
'''
# \d == [0-9]
# \D == [^0-9]
# \w \W
# ip_string = "IP of localhost is: 127.0.0.100 and it is down"
# pattern = re.compile(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
# results = pattern.findall(ip_string)
# print(results)


# https://github.com/Velocidex/WinPmem
# https://extendsclass.com/regex-tester.html#python

