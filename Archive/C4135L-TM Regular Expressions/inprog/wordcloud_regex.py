### Credit: https://www.geeksforgeeks.org/generating-word-cloud-python/
# REQUIREMENTS:
    # Microsoft Visual C++ 14.0 or greater is required. <-- make sure you select the C++ binaries to be installed
    # Get it with "Microsoft C++ Build Tools": https://visualstudio.microsoft.com/visual-cpp-build-tools/
#   wordcloud_requirements.txt should be in the base
#   py -m pip install -r .\wordcloud_requirements.txt 
    # matplotlib
    # pandas
    # wordcloud
    # 

# Python program to generate WordCloud

# importing all necessary modules
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import pandas as pd

# Reads 'Youtube04-Eminem.csv' file using pandas module looking in the base cwd
df = pd.read_csv(r"Youtube04-Eminem.csv", encoding ="latin-1")

comment_words = ''
# STOPWORDS is a set of words inported from a file in the wordcloud package
stopwords = set(STOPWORDS)

# iterate through the csv file
for val in df.CONTENT:
    
    # typecaste each val to string
    val = str(val)

    # split the value
    tokens = val.split()
    
    # Converts each token into lowercase
    for i in range(len(tokens)):
        tokens[i] = tokens[i].lower()
    
    comment_words += " ".join(tokens)+" "

wordcloud = WordCloud(width = 800, height = 800,
                background_color ='white',
                stopwords = stopwords,
                min_font_size = 10).generate(comment_words)

# plot the WordCloud image                    
plt.figure(figsize = (8, 8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)

plt.show()
