{
    "QUESTION": "The data element contains a portion of a BIND DNS log file. Use a regular expression to pull all of the Client IP addresses from the entries.   Client IP Addresses are between the word client and before the pound sign.",
    "ANSWER": "['145.65.52.73', '96.22.134.32', '176.142.230.134', '16.163.27.91', '201.204.7.168', '32.42.56.208', '93.248.119.23', '57.129.87.53', '234.10.232.127', '12.168.59.33', '50.40.3.62', '99.189.112.26', '177.228.200.244', '194.34.106.133']",
    "DATA": "client 145.65.52.73#21708,client 96.22.134.32#54347,client 176.142.230.134#28965,client 16.163.27.91#32889,client 201.204.7.168#52244,client 32.42.56.208#29889,client 93.248.119.23#41121,client 57.129.87.53#65030,client 234.10.232.127#51930,client 12.168.59.33#35264,client 50.40.3.62#18743,client 99.189.112.26#14359,client 177.228.200.244#49708,client 194.34.106.133#23855"
}
{
    "QUESTION": "The data element contains a portion of a BIND DNS log file. Use a regular expression to pull all of the Client IP addresses and the hostname from the entries.   Client IP Addresses are between the word Client and before the pound sign. The hostnames are between the word query: and the word IN. Submit a list of tuples with the IP in the first position and the hostname in the second.",
    "ANSWER": "[('60.151.132.109', 'www.host7644173.com'), ('26.16.23.241', 'www.host443973.com'), ('200.89.204.91', 'www.host8398179.com'), ('37.121.164.129', 'www.host6317244.com'), ('217.76.232.60', 'www.host5420338.com'), ('5.57.89.242', 'www.host5027976.com'), ('69.38.148.44', 'www.host3360870.com'), ('124.89.202.34', 'www.host3497651.com')]",
    "DATA": "client 60.151.132.109#20304: query: www.host7644173.com IN,client 26.16.23.241#17976: query: www.host443973.com IN,client 200.89.204.91#50981: query: www.host8398179.com IN,client 37.121.164.129#18532: query: www.host6317244.com IN,client 217.76.232.60#13808: query: www.host5420338.com IN,client 5.57.89.242#19033: query: www.host5027976.com IN,client 69.38.148.44#19362: query: www.host3360870.com IN,client 124.89.202.34#41650: query: www.host3497651.com IN"
}
{
    "QUESTION": "The data element is string that contains some numbers surounded by special characters. Use a regular expression to extract only the 3 digit numbers.  If 3 digits are part of a larger number such as a 4 digit number you should not include it in the results. In other words, the three digits must be preceeded by and followed by a non-word character. Example '123 1234'",
    "ANSWER": "['886', '199', '467', '611', '238']",
    "DATA": "886!199!1904 85711113*1017181011767 1572 467!1402 1000*1692!15642137*302187021311 1674!1658!1799*1591#9632779319052135#611#51931345**238"
}
{
    "QUESTION": "Sentences end in a punctuation (period, question mark or exclamation mark) followed by two spaces.   Data contains a paragraph with multiple sentences.  Use a regular expression to findall() sentences and submit a list of sentences. ",
    "ANSWER": "['Can I be late tomorrow?  ', 'I have a Dr. Appt at 8:00 a.m. in the morning.  ', 'PI is 3.1415.  ']",
    "DATA": "Can I be late tomorrow?  I have a Dr. Appt at 8:00 a.m. in the morning.  PI is 3.1415.  "
}
{
    "QUESTION": "Extract the SSNs from the data.  A SSN is any series of 9 consecutive digits.  Optionally, it may be a series of 3 digits followed by a space or a dash then 2 digits then a space or a dash followed by 4 digits.  Submit a list of SSNs in the order they appear in the data.  All your SSNs should be in the format XXX-XX-XXXX",
    "ANSWER": "['087-73-6050', '356-65-7183', '811-42-3172', '806-14-6168', '000-45-5433', '671-04-6483', '195-48-0832', '503-75-4412', '978-58-6621', '339-80-2547', '471-09-7192']",
    "DATA": "hphUDnO-IEB-uMMg087 73 6050ImdTrdxDhBHpMiXurxt356-65-7183fpdarLOVTaYPAOArRIxRxZvxnYv  os811 42 3172IvdZir-WMAQHUhM lEGEpP--I JG806146168eUcZkBAP-Qbd1000 45 5433jlvtTjC M-I-LleT bWK-sliyhcM671046483sR -dfY oxFJiCMF oAkfYqvK 195480832cyj TtaeFdpyBCNPV--pdya503-75-4412PWXwuZs DRBIkV978-58-6621NOJ- yCZX-fWWM-Swj-b339-80-2547 ySCIP QguN jyhE471-09-7192eoz-I--gHgdv"
}
{
    "QUESTION": "The data() contains filename of a BIND DNS log and a host name.  Open the file specified from the /home/student/Public/log/dnslogs directory.   The answer is a sorted list of all the client IP addresses in the specified log file that queried that host.",
    "ANSWER": "['114.241.201.224', '14.74.213.34', '141.240.95.116', '156.225.23.217', '165.9.122.100', '170.48.68.63', '175.109.254.83', '192.57.63.222', '199.187.208.169', '211.123.75.5', '214.251.144.134', '222.168.183.220', '224.205.158.216', '228.100.74.149', '231.101.236.133', '24.82.125.105', '241.12.72.22', '253.153.52.223', '28.242.250.238', '30.111.131.107', '30.211.105.219', '4.11.205.194', '43.221.17.93', '45.48.15.164', '56.64.104.99', '62.102.33.54', '69.171.229.12', '74.163.158.115', '79.152.78.155', '8.176.105.112', '84.216.59.34', '93.165.53.61', '97.69.42.116', '99.177.211.84']",
    "DATA": "('13.log', 'www.sans.org')"
}