### Credit: https://www.geeksforgeeks.org/generating-word-cloud-python/
# REQUIREMENTS if using matplotlib:
    # Microsoft Visual C++ 14.0 or greater is required. <-- make sure you select the C++ binaries to be installed
    # Get it with "Microsoft C++ Build Tools": https://visualstudio.microsoft.com/visual-cpp-build-tools/

# built-in modules
import re

# modules that need to be installed 
#   wordcloud_requirements.txt should be in the base
#   py -m pip install -r .\wordcloud_requirements.txt 
    # matplotlib (optional)
    # wordcloud
    # 
from wordcloud import WordCloud, STOPWORDS
# import matplotlib.pyplot as plt


# Reads file_to_open looking in the cwd of the application

def scan_file_text(regex_token=None, print_more=None, filename='romeo_and_juliet.txt'):
    file_to_open = filename

    wordlist = []
    wordset = set(())

    print(type(wordlist))
    with open(file_to_open) as filehandler:
        if regex_token:
            # compiled_regex_pattern = re.compile(regex_token)
            compiled_regex_pattern = re.compile(regex_token,re.IGNORECASE)
            # lets look at each line and see what we can do with it using regex.
            for line in filehandler:
                tempwordlist = compiled_regex_pattern.findall(line.lower())
                # [ wordlist.append(_.lower()) for _ in tempwordlist ]
                [ wordlist.append(_.strip(';-:\'\"!?., \n[]')) for _ in tempwordlist ]
                # [ wordlist.append(_.lower().strip(';-:\'\"!?., \n[]')) for _ in tempwordlist ]
        else:
            # lets look at each line and see what we can do with it not using regex.
            for line in filehandler:
                # strip out all words, make lower case
                tempList = line.strip().lower().split()
                [wordlist.append(_.strip(';-:\'\"!?.,[] ')) for _ in tempList]
                
    wordset = set(wordlist)

    if regex_token:
        print("RegEx search token used:",regex_token)
    print("There are -->",len(wordlist),"<-- words in",file_to_open)
    if print_more:
        print(wordlist)
    print("There are -->",len(wordset),"<-- unique words in",file_to_open)
    if print_more:
        print(wordset)
    return wordlist



# STOPWORDS is a set of words imported from a file in the wordcloud package which will 
# be eliminated from the wordcloud gui
stopwords = set(STOPWORDS)

def create_wordcloud(wordlist, to_file=True):
    text = " ".join(wordlist)
    wordlist_len = len(wordlist)
    stopwords = set(STOPWORDS)
 
    # create wordcloud object
    wc = WordCloud(width = 1000, height = 1000,
                background_color ='white',
                stopwords = stopwords,
                min_font_size = 8)
 
    wc.generate(text)
 
    if to_file:
        # save wordcloud
        wc.to_file("output{}.png".format(wordlist_len))

wordlist = scan_file_text()
create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'\w+')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'\w{6}')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'(\w+\W+){6}')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'([\w\']+\W+){6}')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'([\w\']+\w\W+){6}')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'\w{,5}')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'(\w+\W+){,5}')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'\w{5,6}\W')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'([\w\']{4,5}\w\W+)')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'([\w\']{4,}\w\W+)')
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'\Wromeo\W|\Wjuliet\W|\Wlove\W',True)
# create_wordcloud(wordlist, True)
# wordlist = scan_file_text(r'\W(romeo|juliet|love|hate)\W',True)
# create_wordcloud(wordlist, True)
