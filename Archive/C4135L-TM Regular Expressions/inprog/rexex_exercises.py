"""
1. Recognize the following strings: “bat,” “bit,” “but,” “hat,”
“hit,” or “hut.”

2. Match any pair of words separated by a single space, that is,
first and last names.

3. Match any word and single letter separated by a comma and
single space, as in last name, first initial.

4. Match the set of all valid Python identifiers.

5. Match a street address according to your local format (keep
your regex general enough to match any number of street
words, including the type designation). For example, American
street addresses use the format: 1180 Bordeaux Drive. Make
your regex flexible enough to support multi-word street
names such as: 3120 De la Cruz Boulevard.

6. Match simple Web domain names that begin with “www.”
and end with a “.com” suffix; for example, www.yahoo.com.
Extra Credit: If your regex also supports other high-level
domain names, such as .edu, .net, etc. (for example,
www.foothill.edu).

7. Match the set of all valid e-mail addresses (start with a loose
regex, and then try to tighten it as much as you can, yet
maintain correct functionality). Try to break what we did in class 
and improve it.

8. Match the set of all valid Web site addresses (URLs) (start
with a loose regex, and then try to tighten it as much as you
can, yet maintain correct functionality).Try to break what we did in 
class and improve it.

9. Playing with the type() built-in function that returns an object of type, type with description of the type of object. 
Write a function that:
    (1) uses regex to find the name of the type(some_object) that is returned and print this value to console
        Example:
            >>>returned_type_object = type(100)
            >>>returned_type_object 
            <class 'int'>
        In the example we would use regex to find "int" and nothing else.
            >>>returned_type_object = type(100.01)
            >>>returned_type_object 
            <class 'float'>
        In the example we would use regex to find "float" and nothing else.

    (2) determine if the Base type of the type is of type "type"return True, if it isn't, return the string of its base type, None for anything else.
        Example:  True should be returned
            >>>type(int)
            <class 'type'>

        Example: 'builtin_function_or_method'should be returned
            >>>type(oct)
            <class 'builtin_function_or_method'>
            >>> type(builtin_function_or_method)
            Traceback (most recent call last):
            File "<stdin>", line 1, in <module>
            NameError: name 'builtin_function_or_method' is not defined

        Example: 'NoneType' should be returned.
            >>>type(None)
            <class 'NoneType'>
            type(NoneType)
            Traceback (most recent call last):
            File "<stdin>", line 1, in <module>
            NameError: name 'NoneType' is not defined

Note: the python builtin dictionary containing builtin objects is: __builtins__.__dict__
    >>>type(__builtins__.__dict__)
    <class 'dict'>
This will be a dictionary in which the "key" is the name and the "value" is the object
    >>>__builtins__.__dict__['bool'] 
    <class 'bool'>
    >>>type(bool)
    <class 'type">

Note: Running in debug mode will give you false errors associated with accessing __builtins__.__dict__

10. Processing Dates.  
(1) Search the internet for some dummy logs with date entries.
(2) Create a regex pattern that matches the date entries in these logs and nothing else.
BONUS1: Extract the entire log entries based on date/time and write them to another file in reverse order
BONUS2: Find (or create) two sets of logs with date/time entries that over lap themselves in different formats.
BONUS2: combine these entries in a new file and put their date/time format into a one common standard

"""
# import __builtins__
# [_ for _ in __builtins__.__dict__.itervalues() if isinstance(_, type)]
# for key, value in __builtins__.__dict__.items():
#     # print(key,value,"Type: ",type(value))
#     print(key,':',type(value))
# [ print(_,":",__,'Type:',type(__)) for _, __ in __builtins__.__dict__.items() if isinstance(__, type)]
[ print(_,":",__,'Type:',type(__)) for _, __ in __builtins__.__dict__.items()]