'''
This is the unit test for lab6b. Once your code is completed in lab6b_ex.py 
You can run this unit test to ensure your function is providing the expected 
output per the requirements in the prompt of your lab6b_ex.py. 
'''

import unittest, xmlrunner
from lab6b_sol import *

class StandardCases(unittest.TestCase):
    def test_standard_case(self):
        string = "Hello, \n I can't believe you forgot the broadcast address. \
    It's 255.255.255.255. Also, 99.9.999.999 is not a valid address! \
    You could always contact vernon.meighan.1@us.ourjob.net or \
    juno21@gmail.com,but stop sending e-mail to whodat.22@bengals.nfl, \
    test.suite@gitlab.io, because they will have no idea what you are \
    asking and test@juan.1 is an invalid address. This is an internal \
    address (192.168.1.1), this is a public class C (192.155.01.023), and \
    this is a private class A (10.1.1.30). Some people you can learn from \
    on social media can be found on Twitter and Instagram with these handles: \
    @danetworkguy and @dapyguy. There are some other fun topics like base64 and \
    bytecode that we can discuss later."
        tupleList =[('e-mail addresses', 4), ('IPs', 4), ('Social Media Handles', 2)]
        self.assertEqual(tupleList,get_internet_data(string)) 

class EdgeCases(unittest.TestCase):
    def test_empty_string(self):
        string = ""
        self.assertEqual("ERROR: Empty string passed as argument.", get_internet_data(string))
    def test_no_matches(self):
        string = "Regular expressions are double-edged swords. The more complexity is added, the more difficult it is to solve the problem. That’s why, sometimes, it’s hard to find a regular expression that will match all the cases, and it’s better to use several smaller regex instead."
        self.assertEqual("None",get_internet_data(string))


class BonusCases(unittest.TestCase):
    def bonus_test(self):
        string = "Hello, \n I can't believe you forgot the broadcast address. \
    It's 255.255.255.255. Also, 99.9.999.999 is not a valid address! \
    You could always contact vernon.meighan.1@us.ourjob.net or \
    juno21@gmail.com,but stop sending e-mail to whodat.22@bengals.nfl, \
    test.suite@gitlab.io, because they will have no idea what you are \
    asking and test@juan.1 is an invalid address. This is an internal \
    address (192.168.1.1), this is a public class C (192.155.01.023), and \
    this is a private class A (10.1.1.30). Some people you can learn from \
    on social media can be found on Twitter and Instagram with these handles: \
    @danetworkguy and @dapyguy. You should research base64 decoding to see \
    what I am saying here d2hvQGhlbG0KQGZmYl92ZXJuCmZsa2phQAo=, but just know \
    things like this mean nothing: who@helm flkja@ adjlks kjansdlkn=.\
    Lastly these are examples of bytecode: 48 65 6c 6c 6f 77 20 \
    deadbeef. Have fun!"
        tupleList =[('e-mail addresses', 4), ('IPs', 4), ('Social Media Handles', 2), ('Base64 Strings', 1), ('Bytecode', 1)]
        self.assertEqual(tupleList, get_internet_data(string))


if __name__ == '__main__':
  with open('unittest.xml', 'w') as output:
    unittest.main(
    testRunner=xmlrunner.XMLTestRunner(output=output), 
    failfast=False, 
    buffer=False, 
    catchbreak=False
    )
