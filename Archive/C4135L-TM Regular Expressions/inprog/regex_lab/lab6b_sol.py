testMe='''
Complete the function to meet the following requirements:
- Input: A paragraph will be passed in to your function as an argument
    as a single string.
- The matches will be anything appearing to be an e-mail address 
    (e.g whatever@domain.com), a socialmedia handle (e.g. @username),
    or an IP address(e.g. 151.23.68.254).  
- The function will return a list of tuples of the match type and the
    count of those matches. For example:
    [(e-mail addrresses, 10), (Social Media Handles: 7), (IP addresses: 22)]
- Regex must be used to make the appropriate matches 
- Function must pass all unittests in regex_test.py 
- If an empty string is passed, return 'ERROR: Empty string passed as argument.'
- CHALLENGE: If bytecode is passed return, 'String appears to be bytecode'
- CHALLENGE: Add a fourth category to capture the number of base64 strings
- If no matches are found at all, the function should simply return 'None'

"Hello, \n I can't believe you forgot the broadcast address. \
    It's 255.255.255.255. Also, 99.9.999.999 is not a valid address! \
    You could always contact vernon.meighan.1@us.ourjob.net or \
    juno21@gmail.com,but stop sending e-mail to whodat.22@bengals.nfl, \
    test.suite@gitlab.io, because they will have no idea what you are \
    asking and test@juan.1 is an invalid address. This is an internal \
    address (192.168.1.1), this is a public class C (192.155.01.023), and \
    this is a private class A (10.1.1.30). Some people you can learn from \
    on social media can be found on Twitter and Instagram with these handles: \
    @danetworkguy and @dapyguy. You should research base64 decoding to see \
    what I am saying here d2hvQGhlbG0KQGZmYl92ZXJuCmZsa2phQAo= but just know \
    things like this mean nothing: who@helm flkja@ adjlks kjansdlkn= .\
    Lastly these are examples of bytecode: 48 65 6c 6c 6f 77 20 \
    deadbeef . Have fun!"


'''
import re

def get_internet_data(astring):
    # Check argument for empty string
    if (len(astring) < 1):
        # If empty, inform the user
        return f"ERROR: Empty string passed as argument."
    
    # Configure various patterns for e-mails, IPs, Handles, Base64, and Bytecode strings.
    email = re.compile(r'\S+@\S+\.\w{2,3}')
    ip = re.compile(r'(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{2}|[0-9]{1,2})\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{2}|[0-9]{1,2})\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{2}|[0-9]{1,2})\.(25[0-4]|2[0-4][0-9]|[0-1]?[0-9]{2}|[0-9]{1,2})')
    smhandles = re.compile(r'(\s@\S+)|(^@\S+)')
    base64 = re.compile(r'[-A-Za-z0-9+\/\+\-]{15,}\={0,3}')
    bytecode = re.compile(r'[a-fA-F0-9\s]{12,}')

    # Creating a dictionary connecting the pattern objects with a description
    expressions = [
        {'Description': 'e-mail addresses', 'expression': email},
        {'Description': 'IPs', 'expression': ip},
        {'Description': 'Social Media Handles', 'expression': smhandles},
        {'Description': 'Base64 Strings', 'expression': base64},
        {'Description': 'Bytecode', 'expression': bytecode}
    ]

    statistics = []

    # Iterating through the expressions dictionary to find all matches in astring
    for expression in expressions:
        # if it returns one or more matches...
        if (len(expression['expression'].findall(astring)) > 0):    
            # The tuple of Description and count of matches will be added to the statistics list
            statistics.append((expression['Description'], len(expression['expression'].findall(astring))))
    
    # After iterating through the dictionary if there is one or more entries...
    if (len(statistics) > 0):
        #return the stats list
        return statistics
    else:
        # If it is empty return 'None'
        return f'None'

# print(get_internet_data(testMe))