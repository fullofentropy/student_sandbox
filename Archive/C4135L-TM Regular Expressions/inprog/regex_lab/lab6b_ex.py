'''
Complete the function to meet the following requirements:
- Input: A paragraph will be passed in to your function as an argument
    as a single string.
- The matches will be anything appearing to be an e-mail address 
    (e.g whatever@domain.com), a socialmedia handle (e.g. @username),
    or an IP address(e.g. 151.23.68.254).  
- The function will return a list of tuples of the match type and the
    count of those matches. For example:
    [('e-mail addresses', 4), ('IPs', 4), ('Social Media Handles', 2), ('Base64 Strings', 1), ('Bytecode', 1)]
- Regex must be used to make the appropriate matches 
- Function must pass all unittests in regex_test.py 
- If an empty string is passed, return 'ERROR: Empty string passed as argument.'
- Challenge: If bytecode is passed return, 'String appears to be bytecode'
- Challenge: Add a fourth category to capture the number of base64 strings
- If no matches are found at all, the function should simply return 'None'

Consider the string below to test your function before unit testing:

"Hello, \n I can't believe you forgot the broadcast address.
It's 255.255.255.255. Also, 99.9.999.999 is not a valid address!
You could always contact vernon.meighan.1@us.ourjob.net or
juno21@gmail.com,but stop sending e-mail to whodat.22@bengals.nfl,
test.suite@gitlab.io, because they will have no idea what you are
asking and test@juan.1 is an invalid address. This is an internal
address (192.168.1.1), this is a public class C (192.155.01.023), and
this is a private class A (10.1.1.30). Some people you can learn from
on social media can be found on Twitter and Instagram with these handles:
@danetworkguy and @dapyguy. You should research base64 decoding to see
what I am saying here d2hvQGhlbG0KQGZmYl92ZXJuCmZsa2phQAo= but just know
things like this mean nothing: who@helm flkja@ adjlks kjansdlkn= 
Lastly these are examples of bytecode: 48 65 6c 6c 6f 77 20
deadbeef . Have fun!"

Additionally, one could utilize the online resource of regex101.com to test
your regex.

'''
import re

def get_internet_data(astring):
    pass
