from vehicles_package import Car, SUV, Truck, Automobile

# https://docs.python.org/3/library/csv.html
import csv


def main():
    """
    This program will read a provided CSV file and load it into
    a dictionary for processing.
    """
    
    data_file = 'Book1.csv'
    # data_file = input("Please provide a filename: ") or 'Book1.csv'
    print(f"Searching for {data_file}")

    # Define key name used to find correct vehicle type
    searchKey = 'Type'.lower()

    # Create a dictionary of classes to search with an associated
    # list for adding instances
    vehicleClassInstanceDict = {
        'Car':   (Car, []),
        'SUV':   (SUV, []),
        'Truck': (Truck, [])
        }
    vehicleClassIndex = 0
    vehicleInstanceIndex = 1

    # Open the file for csv processing
    with open(data_file, mode='r') as csv_file:
        """
        CSV file format used:
        First row are the headers for the rest
        ------------------------------------
        Type,Make,Model,Mileage,Price,Doors,Drive_type,pass_cap
        Car,Ford,Torus,40000,10,4,None,None
        Truck,Dodge,Ram,400,20000,None,4x4,None
        SUV,Ford,Escape,50000,45000,None,None,5  
        ------------------------------------
        """

        # Read in the csv file using DictReader 
        # (first line is assumbed to be the key's of the dict)
        readinDict = csv.DictReader(csv_file)

        # iterate through the dictionary for the row in the csv file.
        for dictRow in readinDict:

            # input sanitize the keys to be all lower case
            dictRow = { key.lower(): value for key, value in dictRow.items() }
            
            # if the car car type is in our pre-defined vehicalClassDict, create an instance of it.
            if dictRow[searchKey] in vehicleClassInstanceDict.keys():

                # This is a temp tuple of two a Class, list of instances pair
                tempClassInstanceTuple = vehicleClassInstanceDict[dictRow[searchKey]]

                # This will create an instance of the Class type called and store it in tempInstance
                # tempInstance = Car(**dictRow)  ## if "Type" == "Car"
                # tempInstance = Truck(**dictRow)## if "Type" == "Truck"
                tempInstance = tempClassInstanceTuple[vehicleClassIndex](**dictRow)

                # This will append the instance to the appropriate key type list
                tempClassInstanceTuple[vehicleInstanceIndex].append(tempInstance)

                print(vehicleClassInstanceDict[dictRow[searchKey]][vehicleInstanceIndex][-1])


    for valueList in vehicleClassInstanceDict.values():
        for instance in valueList[vehicleInstanceIndex]:
            instance: Automobile
            print(f'''This is an instance of 
            type: {instance.__class__.__name__} 
            with make: {instance.get_make()}
            model: {instance.get_model()}
            engine: {instance.get_engine()}''')

# Call the main
main()            


