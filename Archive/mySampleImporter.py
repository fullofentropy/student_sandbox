"""
USING MODULES
First, we import the module
AFter the module is imported the modul'es objects are loaded into memory
If simply importing with "import" we have to us "dot" notation to reference module objects.

IMPORTING MODULES
The most common way to import the module is using the import keyword alone
These mdoules are imported from the same dir as your program.
Doing it this way prevents collisions by putting the modules objects into it's own namespace

"""
# import sys
# print (sys.path)
# print("-------")
# [print(_) for _ in sys.path]

# import perfect_cryptography.password_functions as stuff

# print(stuff.encrypt_password('hunter2'))
# print(__name__)
# print(stuff.decrypt_password('uhagre2'))
# print(perfect_cryptography.encrypt_password('hunter2'))
# print(__name__)
# print(perfect_cryptography.decrypt_password('uhagre2'))

import humanpeople
from humanpeople import People

adam2 = humanpeople.People()
eve = humanpeople.Mammal('Woman')
eve.printMe2()
adam = People()
print('------')
# adam.same_name()
# print(eve.__x1)
print(adam._x2)

x = adam.getPrivateVar()

adam.setPrivateVar(adam.getPrivateVar() * 22)

y = adam.getPrivateVar()

print(x,"|",y)

print(adam._x2)

