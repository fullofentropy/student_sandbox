# This program reads all of the values in the sales.txt file

def main():
    # Open the sales.txt file for reading
    sales_file = open('sales.txt', 'r')

    # Read the first line from the file, but don't convert it to a number yet.
    # We need to test for an empty.

    line = sales_file.readline()

    # As long as an empty string is not returned form readline(), continue processing
    while line != '':
        amount = float(line)

        # format and display th amount
        print(format(amount, ',.2f'))

        # Read the next line
        line = sales_file.readline()

    # close the file
    sales_file.close()

# Call the main function
main()