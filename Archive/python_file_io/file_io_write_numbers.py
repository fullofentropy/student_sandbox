
def main():
    # Open a file for writting
    outfile = open("numbers.txt", 'w')

    # get three numbers from the user
    num1 = int(input('Enter a number: ')) or 34
    num2 = int(input('Enter a number: ')) or 3
    num3 = int(input('Enter a number: ')) or 36

    # write the nubmer to the file
    outfile.write(str(num1) + '\n')
    outfile.write(str(num2) + '\n')
    outfile.write(str(num3) + '\n')

    # Close the file
    outfile.close()
    print("Data was written to numbers.txt")

# Call the main
main()