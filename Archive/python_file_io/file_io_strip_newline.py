# This program reads the contents of the philosophers.txt
# file one line at a time.

def main():
    # Open philosophers.txt for reading
    infile = open('philosophers.txt', 'r')

    # Read the lines one by one from the file
    line1 = infile.readline()
    line2 = infile.readline()
    line3 = infile.readline()

    # Strip the \n from each string
    line1 = line1.rstrip()
    line2 = line2.lstrip()
    line3 = line3.strip()
    
    # Close the file
    infile.close()

    # Print the lines we've read
    print(line1)
    print(line2)
    print(line3)
    
# Call the main function
main()