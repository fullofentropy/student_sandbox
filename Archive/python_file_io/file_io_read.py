# This program reads and displays the contents
# of the philosophers.txt file
 
def main():
    # Open a filenames philosophers.txt
    with open('philosophers.txt', 'r') as infile:
        # Read the files contents.
        file_contents = infile.read()

    # # Open a filenames philosophers.txt
    # infile = open('philosophers.txt', 'r')

    # # Read the files contents.
    # file_contents = infile.read()

    # # close the file
    # infile.close()

    # Print the data from the file
    print(file_contents)

# Call the main function
main()


