# This program gets three names from the user and writes them to a file

def main():
    # Get three names
    print('Enter the names of three friends.')
    name1 = input('Friend #1: ') or 'bill'
    name2 = input('Friend #2: ') or 'bob'
    name3 = input('Friend #3: ') or 'jill'

    # Open a file names friends.txt
    with open('friends.txt', 'w') as myfile:

        # Write the names to the file
        myfile.write(name1 + '\n')
        myfile.write(name2 + '\n')
        myfile.write(name3 + '\n')

    # # Open a file names friends.txt
    # myfile = open('friends.txt', 'w')

    # # Write the names to the file
    # myfile.write(name1 + '\n')
    # myfile.write(name2 + '\n')
    # myfile.write(name3 + '\n')

    # # Close the file
    # myfile.close()
    print("The names were written to friends.txt")

# Call the main function
main()

