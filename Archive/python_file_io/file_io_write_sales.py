# this program prompts the user to sales amount
# and writes those amounts to the sales.txt file

def main():
    # Get the number of days.
    try:
        num_days = int(input('For how many days do you have sales? '))

    except ValueError:
        print("Enter integer values only!")
        main()

    # Open a new file names sales.txt
    sales_file = open('sales.txt', 'w')

    # Get the amount of sales for each dayand write it to this file
    for count in range(1, num_days + 1):
        # Get the sales for a day
        sales = float(
            input('Enter the sales fo the day #' + str(count) + ': '))

        # Write the sales amount to the file
        sales_file.write(str(sales) + '\n')

    # Close the file
    sales_file.close()
    print("Data written to sales.txt")


# Call the main
main()
