"""Student Code: Lab3B

Instructions:
    * You are provided with a text file that contains the best song lyrics in the world
    * Problem is... the song lyrics are encrypted with a simple XOR.
    * You will need to decrypt the lyrics
    * The key is 27
    * You have been provided with a decent chunk of the code with conditionals and loops already created...
    * Feel free to create yours from scratch if you want a greater challenge. 
    * You will need to think outside the box. Remember what XOR is, the type of data it acts on, how much data, etc. 

BONUS:  WHEN YOU GET THE ANSWER, IF YOU USED BYTES, RESOLVE IT USING TEXT ENCODING, OR VICE VERSA.

"""
def studentCode():

    # TODO: Create variables and open file here

    # Define the Key
    key = 27

    # Initialize an empty string
    finalString = ""

    # TODO: Read your file   (provided "file.txt")

    # Uncomment below, replacing DATA with your string or bytes to loop through
    """
    for DATA_CHUNK in DATA:
        # Perform XOR on  "DATA_CHUNK" here

    """
    # Replace either the return or reassign your unencrypted string to finalString

    return finalString

studentSTR = studentCode()

print(studentSTR)

