# This program reads the contents of the philosophers.txt
# file one line at a time.

def main():
    # Open philosophers.txt for reading
    infile = open('philosophers.txt', 'r')

    # Read the lines one by one from the file
    line1 = infile.readlines(10000)
    # line2 = infile.readline(2)
    # line3 = infile.readline(50)
    # line4 = infile.readline(3)
    
    # Close the file
    infile.close()

    # Print the lines we've read
    print(line1[1])
    # print(line2)
    # print(line3)
    # print(line4)

# Call the main function
main()