"""
When a program need sto save for later user, it write the data in a file.
The data can from the file directly after or at a later time.

'r'     - open a file for reading
'w'     - opens a file for writing
'a'     - opens a file to be written, appending at the end

'+'     - makes the mode both raead and write (r+) (or write and read w+)
't'     - is implied if not specifically calling out binary mode, adding it doesn't hurt
'b'     - binary mode

example:

fh = open('myfile.text', 'r')  <-- this implies the 't' for text mode
fh = open('myfile.text', 'rt')  

fh = open('myfile.txt', 'rb')

"""

def main():
    # Open a file named philosophers.txt

    with open('philosophers.txt', 'w') as outfile:

        # Write the names of the three philophoers
        outfile.write('Richard Boettcher\n')
        outfile.write('David Hume\n')
        outfile.write('Emmanual Kant\n')

    #############################################
    # outfile = open('philosophers.txt', 'w')

    # # Write the names of the three philophoers
    # outfile.write('John Locke\n')
    # outfile.write('David Hume\n')
    # outfile.write('Emmanual Kant\n')

    # # Close the file
    # outfile.close()

# Call the main function
main()
