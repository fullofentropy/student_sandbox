# This program tries to display the contens of a file that doesn't exist.

def main():

    try:
        # Get the name of the file
        filename = input('Enter the file name: ')

        # Open the file
        infile = open(filename, 'r')

        # read the file's content
        contents = infile.read()


        # Display the file's contents
        print(contents)

    except (FileNotFoundError):
        print("This file is invalid, please try again.")
        main()

    except:
        print("some other error")





# Call the main funciton
main()