"""Student Code: Lab3B

Instructions:
* You are provided with a text file that contains the best song lyrics in the world
    * Problem is... the song lyrics are encrypted with a simple XOR.
* You will need to decrypt the lyrics
    * The key is 27
    * You have been provided with a decent chunk of the code with conditionals and loops already created...
    * Feel free to create yours from scratch if you want a greater challange. 
* You will need to think outside the box. Remember what XOR is, the type of data it acts on, how much data, etc. 
"""
def studentCode():
    # TODO: Create variables and open file here
    myfile = open(r'.\C4115L-TM\File Lessons\lab3B\file.txt', 'rb')

    key = 27
    finalString = ""

    # TODO: Read your file

    mydata = myfile.read()

    # Uncomment below, replacing STR with your string or datatype to loop

# ANSWER WHEN READING FILE AS TEXT
    
    # for i in mydata:
    #     # Perform XOR here
    #     temp = ord(i)^key
    #     finalString = finalString + chr(temp)

# ANSWER WHEN READING FILE AS BYTE

    # for i in mydata:
    #     # Perform XOR here
    #     temp = i^key
    #     finalString = finalString + chr(temp)
 

    return finalString

studentSTR = studentCode()

print(studentSTR)

