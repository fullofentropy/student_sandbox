"""
File IO Problems

1. File Display
Create a file containing a series of integers named numbers.txt. Write a program that 
displays all of the numbers in the file

2. File Head Display
Write a program that asks the user for the name of a file. The program should display only the first 
five lines of the file’s contents. If the file contains less than five lines, it should display the 
file’s entire contents.

3. Line Numbers
Write a program that asks the user for the name of a file. The program should display the
contents of the file with each line preceded with a line number followed by a colon. The
line numbering should start at 1.

4. Item Counter
Assume that a file containing a series of names (as strings) is named names.txt and exists
on the computer’s disk. Write a program that displays the number of names that are stored
in the file. (Hint: Open the file and read every string stored in it. Use a variable to keep a
count of the number of items that are read from the file.)

5. Sum of Numbers
Assume that a file containing a series of integers is named numbers.txt and exists on the computer’s disk. 
Write a program that reads all of the numbers stored in the file and calculates their total.

6. Average of Numbers
Assume that a file containing a series of integers is named numbers.txt and exists on the
computer’s disk. Write a program that calculates the average of all the numbers stored in
the file.

7. Random Number File Writer
Write a program that writes a series of random numbers to a file. Each random number
should be in the range of 1 through 100. The application should let the user specify how
many random numbers the file will hold.

8. Random Number File Reader
This exercise assumes you have completed Programming Exercise 7, Random Number File
Writer. Write another program that reads the random numbers from the file, display the
numbers, and then display the following data:
• The total of the numbers
• The number of random numbers read from the file

9. Exception Handing
Modify the program that you wrote for Exercise 6 so it handles the following exceptions:
• It should handle any IOError exceptions that are raised when the file is opened and data
is read from it.

10. Golf Scores
The Springfork Amateur Golf Club has a tournament every weekend. The club president
has asked you to write two programs:
    1. A program that will read each player’s name and golf score as keyboard input, and then
    save these as records in a file named golf.txt. (Each record will have a field for the
    player’s name and a field for the player’s score.)
    2. A program that reads the records from the golf.txt file and displays them.
    
"""